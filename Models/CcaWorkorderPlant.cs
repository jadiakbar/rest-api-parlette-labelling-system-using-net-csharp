﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaWorkorderPlant
    {
        public long WorkorderId { get; set; }
        public int PlantId { get; set; }
        public long Seqid { get; set; }
    }
}
