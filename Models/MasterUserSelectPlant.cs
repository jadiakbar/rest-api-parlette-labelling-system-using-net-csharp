﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class MasterUserSelectPlant
    {
        public int UserId { get; set; }
        public int PlantId { get; set; }
        public long Seqid { get; set; }
    }
}
