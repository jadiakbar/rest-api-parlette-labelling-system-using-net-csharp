﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaLineBlockstock
    {
        public int PlantId { get; set; }
        public int LineId { get; set; }
        public string IsBlockstock { get; set; }
        public long Seqid { get; set; }
    }
}
