﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaSsccScrapped
    {
        public long SsccId { get; set; }
        public string SsccNumber { get; set; }
        public decimal SsccQty { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
        public long Seqid { get; set; }
    }
}
