﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaFtpProcessorder
    {
        public long FtpprocessId { get; set; }
        public string FtpprocessFile { get; set; }
        public string FtpprocessName { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
    }
}
