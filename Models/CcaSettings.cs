﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaSettings
    {
        public string Option1 { get; set; }
        public string Option2 { get; set; }
        public long Seqid { get; set; }
    }
}
