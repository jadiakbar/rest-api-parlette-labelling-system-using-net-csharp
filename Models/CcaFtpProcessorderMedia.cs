﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaFtpProcessorderMedia
    {
        public long FtpprocessId { get; set; }
        public long MediaId { get; set; }
        public long Seqid { get; set; }
    }
}
