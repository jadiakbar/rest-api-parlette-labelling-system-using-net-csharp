﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaSsccContent
    {
        public long SsccId { get; set; }
        public string PlantName { get; set; }
        public string PalletNumber { get; set; }
        public string ItemMaterialNo { get; set; }
        public DateTime ProdDate { get; set; }
        public string ProdCode { get; set; }
        public string ArticleName { get; set; }
        public decimal ArticleQty { get; set; }
        public DateTime BestBeforeDate { get; set; }
        public string BatchNo { get; set; }
        public string SsccNumber { get; set; }
        public string BarcodeToken { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
        public long Seqid { get; set; }
    }
}
