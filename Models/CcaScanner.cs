﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaScanner
    {
        public int ScannerId { get; set; }
        public string ScannerName { get; set; }
        public string ScannerIp { get; set; }
        public int ScannerPort { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
    }
}
