﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaSsccStart
    {
        public long SsccId { get; set; }
        public string IsStart { get; set; }
        public int RecUser { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
        public long Seqid { get; set; }
    }
}
