﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaServiceLine
    {
        public int ServiceId { get; set; }
        public int LineId { get; set; }
        public int Seqid { get; set; }
    }
}
