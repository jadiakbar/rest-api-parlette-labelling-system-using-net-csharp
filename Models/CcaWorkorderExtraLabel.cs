﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaWorkorderExtraLabel
    {
        public long WorkorderId { get; set; }
        public int ExtralabelId { get; set; }
        public long Seqid { get; set; }
    }
}
