﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class Author
    {
        public Author()
        {
            Book = new HashSet<Book>();
        }

        public Guid AuthorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Genre { get; set; }

        public virtual ICollection<Book> Book { get; set; }
    }
}
