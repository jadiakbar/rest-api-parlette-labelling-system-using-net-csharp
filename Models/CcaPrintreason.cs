﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaPrintreason
    {
        public int PrintreasonId { get; set; }
        public string PrintreasonName { get; set; }
        public string PrintreasonCode { get; set; }
    }
}
