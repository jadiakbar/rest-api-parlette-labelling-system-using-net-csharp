﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaWorkorderLine
    {
        public long WorkorderId { get; set; }
        public int LineId { get; set; }
        public long Seqid { get; set; }
    }
}
