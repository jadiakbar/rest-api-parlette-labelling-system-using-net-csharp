﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaPrinterPlant
    {
        public int? PrinterId { get; set; }
        public int? PlantId { get; set; }
        public long Seqid { get; set; }
    }
}
