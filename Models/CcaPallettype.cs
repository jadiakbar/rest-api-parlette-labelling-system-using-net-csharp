﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaPallettype
    {
        public string PallettypeName { get; set; }
        public string PallettypeDesc { get; set; }
        public long PallettypeId { get; set; }
    }
}
