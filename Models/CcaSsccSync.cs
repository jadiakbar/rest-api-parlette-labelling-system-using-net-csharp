﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaSsccSync
    {
        public long SsccId { get; set; }
        public int IsSynced { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime? RecDatetime { get; set; }
        public long Seqid { get; set; }
    }
}
