﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaWorkorderRegister
    {
        public long WorkorderId { get; set; }
        public int CurrentPallet { get; set; }
        public int LastPallet { get; set; }
        public int NumPallet { get; set; }
        public int StartPallet { get; set; }
        public int EndPallet { get; set; }
        public int RecUser { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
        public long Seqid { get; set; }
    }
}
