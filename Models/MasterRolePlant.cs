﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class MasterRolePlant
    {
        public int? RoleId { get; set; }
        public int? PlantId { get; set; }
        public int Seqid { get; set; }
    }
}
