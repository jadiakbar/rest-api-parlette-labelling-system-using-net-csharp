﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaMedia
    {
        public long MediaId { get; set; }
        public string MediaName { get; set; }
        public string MediaAlias { get; set; }
        public string MediaType { get; set; }
        public decimal MediaSize { get; set; }
        public DateTime MediaCreated { get; set; }
    }
}
