﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaWorkorderPalletType
    {
        public long? WorkorderId { get; set; }
        public long? SsccId { get; set; }
        public string PalletType { get; set; }
        public int? IsScanned { get; set; }
        public DateTime? RecCreated { get; set; }
    }
}
