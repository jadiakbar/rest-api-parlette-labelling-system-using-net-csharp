﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class MasterSessions
    {
        public string SessionId { get; set; }
        public string IpAddress { get; set; }
        public string UserAgent { get; set; }
        public int LastActivity { get; set; }
        public string UserData { get; set; }
        public long Seqid { get; set; }
    }
}
