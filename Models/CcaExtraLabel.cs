﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaExtraLabel
    {
        public long ExtralabelId { get; set; }
        public double QtyLabel { get; set; }
        public double NumCases { get; set; }
        public string PalletNumber { get; set; }
        public int? PalletFrom { get; set; }
        public int? PalletTo { get; set; }
        public string IsReprinted { get; set; }
        public DateTime BestBeforeDate { get; set; }
        public int RecUser { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
    }
}
