﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class MasterSecurity
    {
        public int SecurityId { get; set; }
        public string SecurityName { get; set; }
        public string SecurityDescr { get; set; }
        public string SecurityCode { get; set; }
        public short? Seq { get; set; }
    }
}
