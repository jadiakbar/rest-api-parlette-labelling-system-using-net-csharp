﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaWorkorderEol
    {
        public long WorkorderId { get; set; }
        public string IsEol { get; set; }
        public long Seqid { get; set; }
    }
}
