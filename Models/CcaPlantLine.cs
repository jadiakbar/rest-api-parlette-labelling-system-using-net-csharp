﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaPlantLine
    {
        public int PlantId { get; set; }
        public int LineId { get; set; }
        public int Seqid { get; set; }
    }
}
