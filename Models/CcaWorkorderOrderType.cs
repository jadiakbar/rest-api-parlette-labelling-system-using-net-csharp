﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaWorkorderOrderType
    {
        public long WorkorderId { get; set; }
        public string Ordertype { get; set; }
        public long Seqid { get; set; }
    }
}
