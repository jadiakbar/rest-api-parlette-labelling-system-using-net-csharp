﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class MasterRoleAdmin
    {
        public int RoleId { get; set; }
        public string IsAdmin { get; set; }
        public int Seqid { get; set; }
    }
}
