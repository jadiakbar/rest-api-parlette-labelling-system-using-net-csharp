﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaScannerPlant
    {
        public int? ScannerId { get; set; }
        public int? PlantId { get; set; }
        public long Seqid { get; set; }
    }
}
