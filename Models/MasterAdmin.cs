﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class MasterAdmin
    {
        public int AdminId { get; set; }
        public string AdminName { get; set; }
        public string AdminFullname { get; set; }
        public string AdminEmail { get; set; }
        public string IsActive { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
    }
}
