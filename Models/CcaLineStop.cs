﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaLineStop
    {
        public int LineId { get; set; }
        public string IsStop { get; set; }
        public long Seqid { get; set; }
    }
}
