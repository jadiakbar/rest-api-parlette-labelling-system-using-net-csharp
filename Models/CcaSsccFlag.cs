﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaSsccFlag
    {
        public long LineId { get; set; }
        public DateTime RecCreated { get; set; }
        public int Count { get; set; }
        public int Flag { get; set; }
        public long Seqid { get; set; }
    }
}
