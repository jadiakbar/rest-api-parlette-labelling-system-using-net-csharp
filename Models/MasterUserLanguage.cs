﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class MasterUserLanguage
    {
        public int UserId { get; set; }
        public string Language { get; set; }
        public int Seqid { get; set; }
    }
}
