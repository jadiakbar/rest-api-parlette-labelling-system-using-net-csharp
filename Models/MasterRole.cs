﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class MasterRole
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
    }
}
