﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaWorkorderMedia
    {
        public long WorkorderId { get; set; }
        public long MediaId { get; set; }
        public long Seqid { get; set; }
    }
}
