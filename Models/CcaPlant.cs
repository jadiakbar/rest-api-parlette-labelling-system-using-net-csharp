﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaPlant
    {
        public int PlantId { get; set; }
        public string PlantDescr { get; set; }
        public string PlantCode { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
    }

    public partial class CcaPlantJoin
    {
        public int PlantId { get; set; }
        public string PlantDescr { get; set; }
        public string PlantCode { get; set; }
        public string IsInterval { get; set; }
        public int GrInterval { get; set; }
        public string UseWms { get; set; }
        public string UseEwm { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
    }
}
