﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaPartPalletPrinter
    {
        public long PartpalletId { get; set; }
        public int? PrinterId { get; set; }
        public long Seqid { get; set; }
    }
}
