﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaDevicesOnline
    {
        public int DeviceId { get; set; }
        public string IsOnline { get; set; }
        public int Seqid { get; set; }
    }
}
