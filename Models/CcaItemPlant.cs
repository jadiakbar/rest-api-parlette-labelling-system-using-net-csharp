﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaItemPlant
    {
        public long ItemKey { get; set; }
        public int PlantId { get; set; }
        public long Seqid { get; set; }
    }
}
