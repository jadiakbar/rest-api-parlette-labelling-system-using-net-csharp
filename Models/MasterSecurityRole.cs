﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class MasterSecurityRole
    {
        public int SecurityId { get; set; }
        public int RoleId { get; set; }
        public string Nread { get; set; }
        public string Nwrite { get; set; }
        public long Seqid { get; set; }
    }
}
