﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaItem
    {
        public long ItemKey { get; set; }
        public string ItemName { get; set; }
        public string ItemStarter { get; set; }
        public string ItemEan { get; set; }
        public string ItemSku { get; set; }
        public string ItemMaterialNo { get; set; }
        public int ItemBestBeforeInDays { get; set; }
        public int ItemPalletQty { get; set; }
        public string ItemFlag { get; set; }
        public short? ItemDwnWeek { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
        public string ItemPalletType { get; set; }
        public int? ItemPalletQty2 { get; set; }
        public string ItemPalletType2 { get; set; }
    }

    public partial class CcaItemJoin
    {
        public long ItemKey { get; set; }
        public string ItemName { get; set; }
        public string ItemStarter { get; set; }
        public string ItemEan { get; set; }
        public string ItemSku { get; set; }
        public string ItemMaterialNo { get; set; }
        public int ItemBestBeforeInDays { get; set; }
        public int ItemPalletQty { get; set; }
        public string ItemFlag { get; set; }
        public short? ItemDwnWeek { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
        public string ItemPalletType { get; set; }
        public int? ItemPalletQty2 { get; set; }
        public string ItemPalletType2 { get; set; }
    }
}
