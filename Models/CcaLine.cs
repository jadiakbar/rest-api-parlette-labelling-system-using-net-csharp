﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaLineJoin
    {
        /*
        public int LineId { get; set; }
        public string LineName { get; set; }
        public string LineCode { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
        */
        public int PlantId { get; set; }
        public string PlantDescr { get; set; }
        public string PlantCode { get; set; }
        public string PlantArea { get; set; }
        public int LineId { get; set; }
        public string LineName { get; set; }
        public string LineCode { get; set; }
        public string IsRepack { get; set; }
        public string SsccCode { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }

    }

    public partial class CcaLine
    {
        public int LineId { get; set; }
        public string LineName { get; set; }
        public string LineCode { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
    }
}
