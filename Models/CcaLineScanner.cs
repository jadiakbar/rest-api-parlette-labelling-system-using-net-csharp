﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaLineScanner
    {
        public int LineId { get; set; }
        public int ScannerId { get; set; }
        public long Seqid { get; set; }
    }
}
