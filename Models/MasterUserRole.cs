﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class MasterUserRole
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public long Seqid { get; set; }
    }
}
