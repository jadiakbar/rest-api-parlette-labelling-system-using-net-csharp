﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaLineMockup
    {
        public int LineId { get; set; }
        public string IsMockup { get; set; }
        public long Seqid { get; set; }
    }
}
