﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaPlantEwm
    {
        public int PlantId { get; set; }
        public string UseEwm { get; set; }
        public long Seqid { get; set; }
    }
}
