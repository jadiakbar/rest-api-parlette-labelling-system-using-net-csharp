﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaService
    {
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public string ServiceDescr { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
        //public object PlantId { get; internal set; }
    }
}
