﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaSsccExtraLabel
    {
        public long SsccId { get; set; }
        public int ExtralabelId { get; set; }
        public long Seqid { get; set; }
    }
}
