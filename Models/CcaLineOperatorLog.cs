﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaLineOperatorLog
    {
        public int LineId { get; set; }
        public int UserId { get; set; }
        public DateTime RecCreated { get; set; }
        public int Seqid { get; set; }
    }
}
