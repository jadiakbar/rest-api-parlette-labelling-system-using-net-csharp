﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaLineBlockstockHistory
    {
        public int PlantId { get; set; }
        public int LineId { get; set; }
        public string IsBlockstock { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
        public long Seqid { get; set; }
    }
}
