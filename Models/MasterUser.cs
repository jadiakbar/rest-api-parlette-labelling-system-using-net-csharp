﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class MasterUser
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserFullname { get; set; }
        public string UserPhone { get; set; }
        public string UserEmail { get; set; }
        public string UserStatus { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
    }
}
