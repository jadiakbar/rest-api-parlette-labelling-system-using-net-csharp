﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaWorkorder
    {
        public long WorkorderId { get; set; }
        public string WorkorderNo { get; set; }
        public int ItemKey { get; set; }
        public string BatchNo { get; set; }
        public decimal ItemQty { get; set; }
        public DateTime ProdDate { get; set; }
        public DateTime ExpectedProdDate { get; set; }
        public decimal ExpectedCase { get; set; }
        public decimal ExpectedPallets { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
    }
}
