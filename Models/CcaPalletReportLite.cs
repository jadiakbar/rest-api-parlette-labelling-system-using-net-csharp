﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaPalletReportLiteJoin
    {
        public long SsccId { get; set; }
        public DateTime? PrintCreated { get; set; }
        public DateTime? ScanCreated { get; set; }
        public string PalletNo { get; set; }
        public string Device { get; set; }
        public string ScannerDevice { get; set; }
        public string PrintedByDevice { get; set; }
        public string ScannedByDevice { get; set; }
        public string PrintedBy { get; set; }
        public string PrintedByFull { get; set; }
        public string ScannedBy { get; set; }
        public string ScannedByFull { get; set; }
        public string ReprintedBy { get; set; }
        public string ReprintedByFull { get; set; }
        public string BlockedStock { get; set; }
        public DateTime? SsccReprinted { get; set; }
        public string PrinterDevice { get; set; }
        public string ReprintedReason { get; set; }
        public string PrintreasonCode { get; set; }
        public string PrintreasonName { get; set; }
        public int? RecUser { get; set; }
        public string SessionId { get; set; }
        public long Seqid { get; set; }
        public long WorkorderId { get; set; }
        public string WorkorderNo { get; set; }
        public int? ItemKey { get; set; }
        public string BatchNo { get; set; }
        public decimal ItemQty { get; set; }
        public DateTime? ProdDate { get; set; }
        public DateTime? ExpectedProdDate { get; set; }
        public decimal ExpectedCase { get; set; }
        public decimal ExpectedPallets { get; set; }
        public string SsccNumber { get; set; }
        public decimal SsccQty { get; set; }
        public string PlantName { get; set; }
        public string PalletNumber { get; set; }
        public string ItemMaterialNo { get; set; }
        public string ProdCode { get; set; }
        public string ArticleName { get; set; }
        public decimal ArticleQty { get; set; }
        public DateTime? BestBeforeDate { get; set; }
        public string BarcodeToken { get; set; }
        public string IsReprinted { get; set; }
        public DateTime? RecCreated { get; set; }
    }
        public partial class CcaPalletReportLite
    {
        public long SsccId { get; set; }
        public DateTime? PrintCreated { get; set; }
        public DateTime? ScanCreated { get; set; }
        public string PalletNo { get; set; }
        public string BlockedStock { get; set; }
        public DateTime? SsccReprinted { get; set; }
        public string ReprintedReason { get; set; }
        public string PrintreasonCode { get; set; }
        public string PrintreasonName { get; set; }
        public int? RecUser { get; set; }
        public string SessionId { get; set; }
        public long Seqid { get; set; }
    }
}
