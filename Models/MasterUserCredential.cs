﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class MasterUserCredential
    {
        public int UserId { get; set; }
        public string UserPass { get; set; }
        public string UserSalt { get; set; }
        public long Seqid { get; set; }
    }
}
