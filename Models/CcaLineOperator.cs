﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaLineOperator
    {
        public int LineId { get; set; }
        public int UserId { get; set; }
        public int Seqid { get; set; }
    }
}
