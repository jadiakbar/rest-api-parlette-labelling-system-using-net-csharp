﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaWorkorderPartPallet
    {
        public long WorkorderId { get; set; }
        public long PartpalletId { get; set; }
        public long Seqid { get; set; }
    }
}
