﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaPlantWms
    {
        public int PlantId { get; set; }
        public string UseWms { get; set; }
        public long Seqid { get; set; }
    }
}
