﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaLinePalletSeq
    {
        public int PlantId { get; set; }
        public int LineId { get; set; }
        public int PalletNumber { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
        public long Seqid { get; set; }
    }
}
