﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaFtpItemMedia
    {
        public long FtpitemId { get; set; }
        public long MediaId { get; set; }
        public long Seqid { get; set; }
    }
}
