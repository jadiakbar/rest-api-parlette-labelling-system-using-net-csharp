﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class Book
    {
        public Guid BookId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Guid AuthorId { get; set; }

        public virtual Author Author { get; set; }
    }
}
