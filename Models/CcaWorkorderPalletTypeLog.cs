﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaWorkorderPalletTypeLog
    {
        public long? WorkorderId { get; set; }
        public long? SsccId { get; set; }
        public string PalletType { get; set; }
        public DateTime? RecCreated { get; set; }
        public long Seqid { get; set; }
    }
}
