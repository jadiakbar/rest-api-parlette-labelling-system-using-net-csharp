﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaSsccPrintreason
    {
        public long SsccId { get; set; }
        public int PrintreasonId { get; set; }
        public long Seqid { get; set; }
    }
}
