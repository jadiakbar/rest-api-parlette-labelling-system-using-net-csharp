﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaSsccScan
    {
        public long SsccId { get; set; }
        public string PalletNumber { get; set; }
        public DateTime ProdDatetime { get; set; }
        public int? RecUser { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
        public long Seqid { get; set; }
    }
}
