﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaLinePrinter
    {
        public int LineId { get; set; }
        public int PrinterId { get; set; }
        public long Seqid { get; set; }
    }
}
