﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaFtpItem
    {
        public long FtpitemId { get; set; }
        public string FtpitemFile { get; set; }
        public string FtpitemName { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
    }
}
