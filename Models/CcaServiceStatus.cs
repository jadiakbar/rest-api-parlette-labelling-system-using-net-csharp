﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaServiceStatus
    {
        public int ServiceId { get; set; }
        public string Status { get; set; }
        public int Seqid { get; set; }
    }
}
