﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaLabelLine
    {
        public int LabelId { get; set; }
        public int LineId { get; set; }
        public int Seqid { get; set; }
    }
}
