﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaRolePlant
    {
        public int RoleId { get; set; }
        public int PlantId { get; set; }
        public long Seqid { get; set; }
    }
}
