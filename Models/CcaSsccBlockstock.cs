﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaSsccBlockstock
    {
        public long SsccId { get; set; }
        public string IsBlockstock { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
        public long Seqid { get; set; }
    }
}
