﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaSsccPrinter
    {
        public long SsccId { get; set; }
        public string IsManual { get; set; }
        public string PrinterName { get; set; }
        public long Seqid { get; set; }
    }
}
