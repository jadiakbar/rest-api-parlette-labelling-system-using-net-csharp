﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaPlantRealtime
    {
        public int PlantId { get; set; }
        public string IsInterval { get; set; }
        public int GrInterval { get; set; }
        public long Seqid { get; set; }
    }
}
