﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaPartPallet
    {
        public long PartpalletId { get; set; }
        public decimal? NumCases { get; set; }
        public string PalletNumber { get; set; }
        public DateTime BestBeforeDate { get; set; }
        public string IsEol { get; set; }
        public int RecUser { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
    }
}
