﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaLineRepack
    {
        public int LineId { get; set; }
        public string IsRepack { get; set; }
        public long Seqid { get; set; }
        public string SsccCode { get; set; }
    }
}
