﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaDevices
    {
        public int DeviceId { get; set; }
        public string DeviceGuid { get; set; }
        public string DeviceImei { get; set; }
        public string DeviceSerial { get; set; }
        public string DeviceName { get; set; }
        public string DeviceModel { get; set; }
    }
}
