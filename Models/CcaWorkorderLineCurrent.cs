﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaWorkorderLineCurrent
    {
        public long WorkorderId { get; set; }
        public int PlantId { get; set; }
        public int LineId { get; set; }
        public string RecUser { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
        public long Seqid { get; set; }
    }
}
