﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaLabel
    {
        public int LabelId { get; set; }
        public string LabelName { get; set; }
        public string AsDefault { get; set; }
        public DateTime? RecCreated { get; set; }
        public DateTime? RecDatetime { get; set; }
    }
}
