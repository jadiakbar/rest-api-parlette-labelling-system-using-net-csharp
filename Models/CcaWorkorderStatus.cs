﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaWorkorderStatus
    {
        public long WorkorderId { get; set; }
        public string Status { get; set; }
        public long Seqid { get; set; }
    }
}
