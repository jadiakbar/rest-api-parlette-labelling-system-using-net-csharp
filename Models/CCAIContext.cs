﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Data.SqlClient;
using Microsoft.Data.SqlClient;

namespace PlsAPI.Models
{
    public partial class CCAIContext : DbContext
    {
        public CCAIContext(DbContextOptions<CCAIContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Author> Author { get; set; }
        public virtual DbSet<Book> Book { get; set; }
        public virtual DbSet<CcaDevices> CcaDevices { get; set; }
        public virtual DbSet<CcaDevicesOnline> CcaDevicesOnline { get; set; }
        public virtual DbSet<CcaExtraLabel> CcaExtraLabel { get; set; }
        public virtual DbSet<CcaExtraLabelPrinter> CcaExtraLabelPrinter { get; set; }
        public virtual DbSet<CcaFtpItem> CcaFtpItem { get; set; }
        public virtual DbSet<CcaFtpItemMedia> CcaFtpItemMedia { get; set; }
        public virtual DbSet<CcaFtpProcessorder> CcaFtpProcessorder { get; set; }
        public virtual DbSet<CcaFtpProcessorderMedia> CcaFtpProcessorderMedia { get; set; }
        public virtual DbSet<CcaItem> CcaItem { get; set; }
        public virtual DbSet<CcaItemJoin> CcaItemJoin { get; set; }
        public virtual DbSet<CcaItemBackup> CcaItemBackup { get; set; }
        public virtual DbSet<CcaItemModby> CcaItemModby { get; set; }
        public virtual DbSet<CcaItemPlant> CcaItemPlant { get; set; }
        public virtual DbSet<CcaLabel> CcaLabel { get; set; }
        public virtual DbSet<CcaLabelLine> CcaLabelLine { get; set; }
        public virtual DbSet<CcaLine> CcaLine { get; set; }
        public virtual DbSet<CcaLineJoin> CcaLineJoin { get; set; }
        public virtual DbSet<CcaLineBlockstock> CcaLineBlockstock { get; set; }
        public virtual DbSet<CcaLineBlockstockHistory> CcaLineBlockstockHistory { get; set; }
        public virtual DbSet<CcaLineMockup> CcaLineMockup { get; set; }
        public virtual DbSet<CcaLineOperator> CcaLineOperator { get; set; }
        public virtual DbSet<CcaLineOperatorLog> CcaLineOperatorLog { get; set; }
        public virtual DbSet<CcaLinePalletSeq> CcaLinePalletSeq { get; set; }
        public virtual DbSet<CcaLinePrinter> CcaLinePrinter { get; set; }
        public virtual DbSet<CcaLineRepack> CcaLineRepack { get; set; }
        public virtual DbSet<CcaLineScanner> CcaLineScanner { get; set; }
        public virtual DbSet<CcaLineStop> CcaLineStop { get; set; }
        public virtual DbSet<CcaMedia> CcaMedia { get; set; }
        public virtual DbSet<CcaOrderdata> CcaOrderdata { get; set; }
        public virtual DbSet<CcaPalletReport> CcaPalletReport { get; set; }
        public virtual DbSet<CcaPalletReportJoin> CcaPalletReportJoin { get; set; }
        public virtual DbSet<CcaPalletReportLite> CcaPalletReportLite { get; set; }
        public virtual DbSet<CcaPalletReportLiteJoin> CcaPalletReportLiteJoin { get; set; }
        public virtual DbSet<CcaPallettype> CcaPallettype { get; set; }
        public virtual DbSet<CcaPartPallet> CcaPartPallet { get; set; }
        public virtual DbSet<CcaPartPalletPrinter> CcaPartPalletPrinter { get; set; }
        public virtual DbSet<CcaPlant> CcaPlant { get; set; }
        public virtual DbSet<CcaPlantJoin> CcaPlantJoin { get; set; }
        public virtual DbSet<CcaPlantEwm> CcaPlantEwm { get; set; }
        public virtual DbSet<CcaPlantLine> CcaPlantLine { get; set; }
        public virtual DbSet<CcaPlantRealtime> CcaPlantRealtime { get; set; }
        public virtual DbSet<CcaPlantWms> CcaPlantWms { get; set; }
        public virtual DbSet<CcaPrinter> CcaPrinter { get; set; }
        public virtual DbSet<CcaPrinterPlant> CcaPrinterPlant { get; set; }
        public virtual DbSet<CcaPrintreason> CcaPrintreason { get; set; }
        public virtual DbSet<CcaRolePlant> CcaRolePlant { get; set; }
        public virtual DbSet<CcaScanner> CcaScanner { get; set; }
        public virtual DbSet<CcaScannerPlant> CcaScannerPlant { get; set; }
        public virtual DbSet<CcaService> CcaService { get; set; }
        public virtual DbSet<CcaServiceLine> CcaServiceLine { get; set; }
        public virtual DbSet<CcaServiceStatus> CcaServiceStatus { get; set; }
        public virtual DbSet<CcaSettings> CcaSettings { get; set; }
        public virtual DbSet<CcaSscc> CcaSscc { get; set; }
        public virtual DbSet<CcaSsccBlockstock> CcaSsccBlockstock { get; set; }
        public virtual DbSet<CcaSsccContent> CcaSsccContent { get; set; }
        public virtual DbSet<CcaSsccDevice> CcaSsccDevice { get; set; }
        public virtual DbSet<CcaSsccExtraLabel> CcaSsccExtraLabel { get; set; }
        public virtual DbSet<CcaSsccFlag> CcaSsccFlag { get; set; }
        public virtual DbSet<CcaSsccMobile> CcaSsccMobile { get; set; }
        public virtual DbSet<CcaSsccPrint> CcaSsccPrint { get; set; }
        public virtual DbSet<CcaSsccPrinter> CcaSsccPrinter { get; set; }
        public virtual DbSet<CcaSsccPrintreason> CcaSsccPrintreason { get; set; }
        public virtual DbSet<CcaSsccQueue> CcaSsccQueue { get; set; }
        public virtual DbSet<CcaSsccReprinted> CcaSsccReprinted { get; set; }
        public virtual DbSet<CcaSsccScan> CcaSsccScan { get; set; }
        public virtual DbSet<CcaSsccScrapped> CcaSsccScrapped { get; set; }
        public virtual DbSet<CcaSsccStart> CcaSsccStart { get; set; }
        public virtual DbSet<CcaSsccSync> CcaSsccSync { get; set; }
        public virtual DbSet<CcaWorkorder> CcaWorkorder { get; set; }
        public virtual DbSet<CcaWorkorderEol> CcaWorkorderEol { get; set; }
        public virtual DbSet<CcaWorkorderExtraLabel> CcaWorkorderExtraLabel { get; set; }
        public virtual DbSet<CcaWorkorderLine> CcaWorkorderLine { get; set; }
        public virtual DbSet<CcaWorkorderLineCurrent> CcaWorkorderLineCurrent { get; set; }
        public virtual DbSet<CcaWorkorderMedia> CcaWorkorderMedia { get; set; }
        public virtual DbSet<CcaWorkorderOrderType> CcaWorkorderOrderType { get; set; }
        public virtual DbSet<CcaWorkorderPalletType> CcaWorkorderPalletType { get; set; }
        public virtual DbSet<CcaWorkorderPalletTypeLog> CcaWorkorderPalletTypeLog { get; set; }
        public virtual DbSet<CcaWorkorderPartPallet> CcaWorkorderPartPallet { get; set; }
        public virtual DbSet<CcaWorkorderPlant> CcaWorkorderPlant { get; set; }
        public virtual DbSet<CcaWorkorderRegister> CcaWorkorderRegister { get; set; }
        public virtual DbSet<CcaWorkorderRegisterHistory> CcaWorkorderRegisterHistory { get; set; }
        public virtual DbSet<CcaWorkorderSscc> CcaWorkorderSscc { get; set; }
        public virtual DbSet<CcaWorkorderStatus> CcaWorkorderStatus { get; set; }
        public virtual DbSet<MasterAdmin> MasterAdmin { get; set; }
        public virtual DbSet<MasterAdminCredential> MasterAdminCredential { get; set; }
        public virtual DbSet<MasterRole> MasterRole { get; set; }
        public virtual DbSet<MasterRoleAdmin> MasterRoleAdmin { get; set; }
        public virtual DbSet<MasterRolePlant> MasterRolePlant { get; set; }
        public virtual DbSet<MasterSecurity> MasterSecurity { get; set; }
        public virtual DbSet<MasterSecurityRole> MasterSecurityRole { get; set; }
        public virtual DbSet<MasterSessions> MasterSessions { get; set; }
        public virtual DbSet<MasterUser> MasterUser { get; set; }
        public virtual DbSet<MasterUserCredential> MasterUserCredential { get; set; }
        public virtual DbSet<MasterUserLanguage> MasterUserLanguage { get; set; }
        public virtual DbSet<MasterUserPlant> MasterUserPlant { get; set; }
        public virtual DbSet<MasterUserRole> MasterUserRole { get; set; }
        public virtual DbSet<MasterUserSelectPlant> MasterUserSelectPlant { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            if (!optionsBuilder.IsConfigured)
            {
                //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=PC-NAME;Database=db_name;User ID=sa;Password=your_password;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Author>(entity =>
            {
                entity.Property(e => e.AuthorId).ValueGeneratedNever();

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Genre)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Book>(entity =>
            {
                entity.HasIndex(e => e.AuthorId);

                entity.Property(e => e.BookId).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(200);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.HasOne(d => d.Author)
                    .WithMany(p => p.Book)
                    .HasForeignKey(d => d.AuthorId);
            });

            modelBuilder.Entity<CcaDevices>(entity =>
            {
                entity.HasKey(e => e.DeviceId)
                    .HasName("PK__cca_devi__3B085D8B3117D1E2");

                entity.ToTable("cca_devices");

                entity.Property(e => e.DeviceId).HasColumnName("device_id");

                entity.Property(e => e.DeviceGuid)
                    .HasColumnName("device_guid")
                    .HasMaxLength(50);

                entity.Property(e => e.DeviceImei)
                    .IsRequired()
                    .HasColumnName("device_imei")
                    .HasMaxLength(30);

                entity.Property(e => e.DeviceModel)
                    .IsRequired()
                    .HasColumnName("device_model")
                    .HasMaxLength(20);

                entity.Property(e => e.DeviceName)
                    .IsRequired()
                    .HasColumnName("device_name")
                    .HasMaxLength(100);

                entity.Property(e => e.DeviceSerial)
                    .IsRequired()
                    .HasColumnName("device_serial")
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<CcaDevicesOnline>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_devices_online");

                entity.HasIndex(e => e.DeviceId)
                    .HasName("IX_cca_devices_online");

                entity.HasIndex(e => e.IsOnline)
                    .HasName("IX_cca_devices_online_1");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.DeviceId).HasColumnName("device_id");

                entity.Property(e => e.IsOnline)
                    .IsRequired()
                    .HasColumnName("is_online")
                    .HasMaxLength(1)
                    .IsFixedLength();
            });

            modelBuilder.Entity<CcaExtraLabel>(entity =>
            {
                entity.HasKey(e => e.ExtralabelId);

                entity.ToTable("cca_extra_label");

                entity.HasIndex(e => e.RecUser)
                    .HasName("IX_cca_extra_label");

                entity.Property(e => e.ExtralabelId).HasColumnName("extralabel_id");

                entity.Property(e => e.BestBeforeDate)
                    .HasColumnName("best_before_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.IsReprinted)
                    .HasColumnName("is_reprinted")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.NumCases).HasColumnName("num_cases");

                entity.Property(e => e.PalletFrom).HasColumnName("pallet_from");

                entity.Property(e => e.PalletNumber)
                    .IsRequired()
                    .HasColumnName("pallet_number")
                    .HasColumnType("text");

                entity.Property(e => e.PalletTo).HasColumnName("pallet_to");

                entity.Property(e => e.QtyLabel).HasColumnName("qty_label");

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecUser).HasColumnName("rec_user");
            });

            modelBuilder.Entity<CcaExtraLabelPrinter>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_extra_label_printer");

                entity.HasIndex(e => e.ExtralabelId)
                    .HasName("IX_cca_extra_label_printer");

                entity.HasIndex(e => e.PrinterId)
                    .HasName("IX_cca_extra_label_printer_1");

                entity.HasIndex(e => new { e.ExtralabelId, e.PrinterId })
                    .HasName("IX_cca_extra_label_printer_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.ExtralabelId).HasColumnName("extralabel_id");

                entity.Property(e => e.PrinterId).HasColumnName("printer_id");
            });

            modelBuilder.Entity<CcaFtpItem>(entity =>
            {
                entity.HasKey(e => e.FtpitemId)
                    .HasName("PK_cca_ftp_item_1");

                entity.ToTable("cca_ftp_item");

                entity.Property(e => e.FtpitemId).HasColumnName("ftpitem_id");

                entity.Property(e => e.FtpitemFile)
                    .IsRequired()
                    .HasColumnName("ftpitem_file")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FtpitemName)
                    .IsRequired()
                    .HasColumnName("ftpitem_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<CcaFtpItemMedia>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_ftp_item_media");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.FtpitemId).HasColumnName("ftpitem_id");

                entity.Property(e => e.MediaId).HasColumnName("media_id");
            });

            modelBuilder.Entity<CcaFtpProcessorder>(entity =>
            {
                entity.HasKey(e => e.FtpprocessId)
                    .HasName("PK__cca_ftp___D31D722A53B7D19F_1");

                entity.ToTable("cca_ftp_processorder");

                entity.Property(e => e.FtpprocessId).HasColumnName("ftpprocess_id");

                entity.Property(e => e.FtpprocessFile)
                    .IsRequired()
                    .HasColumnName("ftpprocess_file")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FtpprocessName)
                    .IsRequired()
                    .HasColumnName("ftpprocess_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<CcaFtpProcessorderMedia>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_ftp_processorder_media");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.FtpprocessId).HasColumnName("ftpprocess_id");

                entity.Property(e => e.MediaId).HasColumnName("media_id");
            });

            modelBuilder.Entity<CcaItem>(entity =>
            {
                entity.HasKey(e => e.ItemKey)
                    .HasName("PK__cca_item__37A71F7E0ED482A6_1");

                entity.ToTable("cca_item");

                entity.HasIndex(e => e.ItemMaterialNo)
                    .HasName("IX_cca_item");

                entity.Property(e => e.ItemKey).HasColumnName("item_key");

                entity.Property(e => e.ItemBestBeforeInDays).HasColumnName("item_best_before_in_days");

                entity.Property(e => e.ItemDwnWeek).HasColumnName("item_dwn_week");

                entity.Property(e => e.ItemEan)
                    .IsRequired()
                    .HasColumnName("item_ean")
                    .HasMaxLength(30);

                entity.Property(e => e.ItemFlag)
                    .HasColumnName("item_flag")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.ItemMaterialNo)
                    .IsRequired()
                    .HasColumnName("item_material_no")
                    .HasMaxLength(10);

                entity.Property(e => e.ItemName)
                    .IsRequired()
                    .HasColumnName("item_name")
                    .HasMaxLength(50);

                entity.Property(e => e.ItemPalletQty).HasColumnName("item_pallet_qty");

                entity.Property(e => e.ItemPalletQty2).HasColumnName("item_pallet_qty_2");

                entity.Property(e => e.ItemPalletType)
                    .HasColumnName("item_pallet_type")
                    .HasMaxLength(5);

                entity.Property(e => e.ItemPalletType2)
                    .HasColumnName("item_pallet_type_2")
                    .HasMaxLength(5);

                entity.Property(e => e.ItemSku)
                    .IsRequired()
                    .HasColumnName("item_sku")
                    .HasMaxLength(10);

                entity.Property(e => e.ItemStarter)
                    .IsRequired()
                    .HasColumnName("item_starter")
                    .HasMaxLength(5);

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<CcaItemJoin>(entity =>
            {
                entity.HasNoKey();
            });

            modelBuilder.Entity<CcaItemBackup>(entity =>
            {
                entity.HasKey(e => e.ItemKey);

                entity.ToTable("cca_item_backup");

                entity.Property(e => e.ItemKey).HasColumnName("item_key");

                entity.Property(e => e.ItemBestBeforeInDays).HasColumnName("item_best_before_in_days");

                entity.Property(e => e.ItemDwnWeek).HasColumnName("item_dwn_week");

                entity.Property(e => e.ItemEan)
                    .IsRequired()
                    .HasColumnName("item_ean")
                    .HasMaxLength(30);

                entity.Property(e => e.ItemFlag)
                    .HasColumnName("item_flag")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.ItemMaterialNo)
                    .IsRequired()
                    .HasColumnName("item_material_no")
                    .HasMaxLength(10);

                entity.Property(e => e.ItemName)
                    .IsRequired()
                    .HasColumnName("item_name")
                    .HasMaxLength(50);

                entity.Property(e => e.ItemPalletQty).HasColumnName("item_pallet_qty");

                entity.Property(e => e.ItemSku)
                    .IsRequired()
                    .HasColumnName("item_sku")
                    .HasMaxLength(10);

                entity.Property(e => e.ItemStarter)
                    .IsRequired()
                    .HasColumnName("item_starter")
                    .HasMaxLength(5);

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<CcaItemModby>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_item_modby");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.ItemKey).HasColumnName("item_key");

                entity.Property(e => e.ModifiedBy).HasColumnName("modified_by");

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<CcaItemPlant>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_item_plant");

                entity.HasIndex(e => e.ItemKey)
                    .HasName("IX_cca_item_plant");

                entity.HasIndex(e => e.PlantId)
                    .HasName("IX_cca_item_plant_1");

                entity.HasIndex(e => new { e.PlantId, e.ItemKey })
                    .HasName("IX_cca_item_plant_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.ItemKey).HasColumnName("item_key");

                entity.Property(e => e.PlantId).HasColumnName("plant_id");
            });

            modelBuilder.Entity<CcaLabel>(entity =>
            {
                entity.HasKey(e => e.LabelId);

                entity.ToTable("cca_label");

                entity.Property(e => e.LabelId).HasColumnName("label_id");

                entity.Property(e => e.AsDefault)
                    .HasColumnName("as_default")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.LabelName)
                    .HasColumnName("label_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<CcaLabelLine>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_label_line");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.LabelId).HasColumnName("label_id");

                entity.Property(e => e.LineId).HasColumnName("line_id");
            });

            modelBuilder.Entity<CcaLine>(entity =>
            {
                entity.HasKey(e => e.LineId)
                    .HasName("PK__cca_line__F5AE5F6266CF8F98");

                entity.ToTable("cca_line");

                entity.HasIndex(e => e.LineCode)
                    .HasName("IX_cca_line");

                entity.Property(e => e.LineId).HasColumnName("line_id");

                entity.Property(e => e.LineCode)
                    .IsRequired()
                    .HasColumnName("line_code")
                    .HasMaxLength(30);

                entity.Property(e => e.LineName)
                    .IsRequired()
                    .HasColumnName("line_name")
                    .HasMaxLength(50);

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<CcaLineJoin>(entity =>
            {
                entity.HasNoKey();
            });

            modelBuilder.Entity<CcaLineBlockstock>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_line_blockstock");

                entity.HasIndex(e => e.LineId)
                    .HasName("IX_cca_line_blockstock_1");

                entity.HasIndex(e => e.PlantId)
                    .HasName("IX_cca_line_blockstock");

                entity.HasIndex(e => new { e.PlantId, e.LineId })
                    .HasName("IX_cca_line_blockstock_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.IsBlockstock)
                    .IsRequired()
                    .HasColumnName("is_blockstock")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.LineId).HasColumnName("line_id");

                entity.Property(e => e.PlantId).HasColumnName("plant_id");
            });

            modelBuilder.Entity<CcaLineBlockstockHistory>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_line_blockstock_history");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.IsBlockstock)
                    .IsRequired()
                    .HasColumnName("is_blockstock")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.LineId).HasColumnName("line_id");

                entity.Property(e => e.PlantId).HasColumnName("plant_id");

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<CcaLineMockup>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_line_mockup");

                entity.HasIndex(e => e.LineId)
                    .HasName("IX_cca_line_mockup");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.IsMockup)
                    .IsRequired()
                    .HasColumnName("is_mockup")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.LineId).HasColumnName("line_id");
            });

            modelBuilder.Entity<CcaLineOperator>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_line_operator");

                entity.HasIndex(e => e.LineId)
                    .HasName("IX_cca_line_operator");

                entity.HasIndex(e => e.UserId)
                    .HasName("IX_cca_line_operator_1");

                entity.HasIndex(e => new { e.LineId, e.UserId })
                    .HasName("IX_cca_line_operator_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.LineId).HasColumnName("line_id");

                entity.Property(e => e.UserId).HasColumnName("user_id");
            });

            modelBuilder.Entity<CcaLineOperatorLog>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_line_operator_log");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.LineId).HasColumnName("line_id");

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("user_id");
            });

            modelBuilder.Entity<CcaLinePalletSeq>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_line_pallet_seq");

                entity.HasIndex(e => e.LineId)
                    .HasName("IX_cca_line_pallet_seq_1");

                entity.HasIndex(e => e.PalletNumber)
                    .HasName("IX_cca_line_pallet_seq_2");

                entity.HasIndex(e => e.PlantId)
                    .HasName("IX_cca_line_pallet_seq");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.LineId).HasColumnName("line_id");

                entity.Property(e => e.PalletNumber).HasColumnName("pallet_number");

                entity.Property(e => e.PlantId).HasColumnName("plant_id");

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<CcaLinePrinter>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_line_printer");

                entity.HasIndex(e => e.LineId)
                    .HasName("IX_cca_line_printer");

                entity.HasIndex(e => e.PrinterId)
                    .HasName("IX_cca_line_printer_1");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.LineId).HasColumnName("line_id");

                entity.Property(e => e.PrinterId).HasColumnName("printer_id");
            });

            modelBuilder.Entity<CcaLineRepack>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_line_repack");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.IsRepack)
                    .IsRequired()
                    .HasColumnName("is_repack")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.LineId).HasColumnName("line_id");

                entity.Property(e => e.SsccCode)
                    .HasColumnName("sscc_code")
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CcaLineScanner>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_line_scanner");

                entity.HasIndex(e => e.LineId)
                    .HasName("IX_cca_line_scanner");

                entity.HasIndex(e => e.ScannerId)
                    .HasName("IX_cca_line_scanner_1");

                entity.HasIndex(e => new { e.LineId, e.ScannerId })
                    .HasName("IX_cca_line_scanner_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.LineId).HasColumnName("line_id");

                entity.Property(e => e.ScannerId).HasColumnName("scanner_id");
            });

            modelBuilder.Entity<CcaLineStop>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_line_stop");

                entity.HasIndex(e => e.LineId)
                    .HasName("IX_cca_line_stop");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.IsStop)
                    .IsRequired()
                    .HasColumnName("is_stop")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.LineId).HasColumnName("line_id");
            });

            modelBuilder.Entity<CcaMedia>(entity =>
            {
                entity.HasKey(e => e.MediaId)
                    .HasName("PK__cca_medi__D0A840F449AD77B6_3");

                entity.ToTable("cca_media");

                entity.Property(e => e.MediaId).HasColumnName("media_id");

                entity.Property(e => e.MediaAlias)
                    .IsRequired()
                    .HasColumnName("media_alias")
                    .HasMaxLength(400);

                entity.Property(e => e.MediaCreated)
                    .HasColumnName("media_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.MediaName)
                    .IsRequired()
                    .HasColumnName("media_name")
                    .HasMaxLength(400);

                entity.Property(e => e.MediaSize)
                    .HasColumnName("media_size")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.MediaType)
                    .IsRequired()
                    .HasColumnName("media_type")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<CcaOrderdata>(entity =>
            {
                entity.HasKey(e => e.OrderdataId)
                    .HasName("PK__cca_orde__48FD388CC42C0816");

                entity.ToTable("cca_orderdata");

                entity.Property(e => e.OrderdataId).HasColumnName("orderdata_id");

                entity.Property(e => e.OrderEnder)
                    .IsRequired()
                    .HasColumnName("order_ender")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.OrderHeader)
                    .IsRequired()
                    .HasColumnName("order_header")
                    .HasMaxLength(5);

                entity.Property(e => e.OrderLine)
                    .HasColumnName("order_line")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OrderMaterial)
                    .IsRequired()
                    .HasColumnName("order_material")
                    .HasMaxLength(10);

                entity.Property(e => e.OrderPlant)
                    .IsRequired()
                    .HasColumnName("order_plant")
                    .HasMaxLength(5);

                entity.Property(e => e.OrderProcess)
                    .IsRequired()
                    .HasColumnName("order_process")
                    .HasMaxLength(10);

                entity.Property(e => e.OrderProddate)
                    .HasColumnName("order_proddate")
                    .HasColumnType("datetime");

                entity.Property(e => e.OrderQtyorder)
                    .HasColumnName("order_qtyorder")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<CcaPalletReport>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_pallet_report");

                entity.HasIndex(e => e.RecUser)
                    .HasName("IX_cca_pallet_report_1");

                entity.HasIndex(e => e.SessionId)
                    .HasName("IX_cca_pallet_report");

                entity.HasIndex(e => new { e.SessionId, e.RecUser })
                    .HasName("IX_cca_pallet_report_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.BlockedStock)
                    .HasColumnName("blocked_stock")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.Device)
                    .HasColumnName("device")
                    .HasMaxLength(50);

                entity.Property(e => e.PalletNo)
                    .HasColumnName("pallet_no")
                    .HasMaxLength(10);

                entity.Property(e => e.PrintCreated)
                    .HasColumnName("print_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrintedBy)
                    .HasColumnName("printed_by")
                    .HasMaxLength(50);

                entity.Property(e => e.PrintedByDevice)
                    .HasColumnName("printed_by_device")
                    .HasMaxLength(50);

                entity.Property(e => e.PrintedByFull)
                    .HasColumnName("printed_by_full")
                    .HasMaxLength(50);

                entity.Property(e => e.PrinterDevice)
                    .HasColumnName("printer_device")
                    .HasMaxLength(50);

                entity.Property(e => e.PrintreasonCode)
                    .HasColumnName("printreason_code")
                    .HasMaxLength(10);

                entity.Property(e => e.PrintreasonName)
                    .HasColumnName("printreason_name")
                    .HasMaxLength(50);

                entity.Property(e => e.RecUser).HasColumnName("rec_user");

                entity.Property(e => e.ReprintedBy)
                    .HasColumnName("reprinted_by")
                    .HasMaxLength(50);

                entity.Property(e => e.ReprintedByFull)
                    .HasColumnName("reprinted_by_full")
                    .HasMaxLength(50);

                entity.Property(e => e.ReprintedReason)
                    .HasColumnName("reprinted_reason")
                    .HasMaxLength(200);

                entity.Property(e => e.ScanCreated)
                    .HasColumnName("scan_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.ScannedBy)
                    .HasColumnName("scanned_by")
                    .HasMaxLength(50);

                entity.Property(e => e.ScannedByDevice)
                    .HasColumnName("scanned_by_device")
                    .HasMaxLength(50);

                entity.Property(e => e.ScannedByFull)
                    .HasColumnName("scanned_by_full")
                    .HasMaxLength(50);

                entity.Property(e => e.ScannerDevice)
                    .HasColumnName("scanner_device")
                    .HasMaxLength(50);

                entity.Property(e => e.SessionId)
                    .HasColumnName("session_id")
                    .HasMaxLength(50);

                entity.Property(e => e.SsccId).HasColumnName("sscc_id");

                entity.Property(e => e.SsccReprinted)
                    .HasColumnName("sscc_reprinted")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<CcaPalletReportJoin>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.ArticleQty)
                    .HasColumnName("ArticleQty")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.ExpectedCase)
                    .HasColumnName("ExpectedCase")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.ExpectedPallets)
                    .HasColumnName("ExpectedPallets")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.ItemQty)
                    .HasColumnName("ItemQty")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.SsccQty)
                    .HasColumnName("SsccQty")
                    .HasColumnType("numeric(18, 0)");
            });

            modelBuilder.Entity<CcaPalletReportLite>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_pallet_report_lite");

                entity.HasIndex(e => e.SsccId)
                    .HasName("IX_cca_pallet_report_lite");

                entity.HasIndex(e => new { e.RecUser, e.SessionId })
                    .HasName("IX_cca_pallet_report_lite_1");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.BlockedStock)
                    .HasColumnName("blocked_stock")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.PalletNo)
                    .HasColumnName("pallet_no")
                    .HasMaxLength(10);

                entity.Property(e => e.PrintCreated)
                    .HasColumnName("print_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrintreasonCode)
                    .HasColumnName("printreason_code")
                    .HasMaxLength(10);

                entity.Property(e => e.PrintreasonName)
                    .HasColumnName("printreason_name")
                    .HasMaxLength(50);

                entity.Property(e => e.RecUser).HasColumnName("rec_user");

                entity.Property(e => e.ReprintedReason)
                    .HasColumnName("reprinted_reason")
                    .HasMaxLength(200);

                entity.Property(e => e.ScanCreated)
                    .HasColumnName("scan_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.SessionId)
                    .HasColumnName("session_id")
                    .HasMaxLength(50);

                entity.Property(e => e.SsccId).HasColumnName("sscc_id");

                entity.Property(e => e.SsccReprinted)
                    .HasColumnName("sscc_reprinted")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<CcaPalletReportLiteJoin>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.ArticleQty)
                    .HasColumnName("ArticleQty")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.ExpectedCase)
                    .HasColumnName("ExpectedCase")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.ExpectedPallets)
                    .HasColumnName("ExpectedPallets")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.ItemQty)
                    .HasColumnName("ItemQty")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.SsccQty)
                    .HasColumnName("SsccQty")
                    .HasColumnType("numeric(18, 0)");
            });

            modelBuilder.Entity<CcaPallettype>(entity =>
            {
                entity.HasKey(e => e.PallettypeId);

                entity.ToTable("cca_pallettype");

                entity.Property(e => e.PallettypeId).HasColumnName("pallettype_id");

                entity.Property(e => e.PallettypeDesc)
                    .HasColumnName("pallettype_desc")
                    .HasMaxLength(50);

                entity.Property(e => e.PallettypeName)
                    .HasColumnName("pallettype_name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<CcaPartPallet>(entity =>
            {
                entity.HasKey(e => e.PartpalletId)
                    .HasName("PK__cca_part__F78860B42C14408A");

                entity.ToTable("cca_part_pallet");

                entity.HasIndex(e => e.RecUser)
                    .HasName("IX_cca_part_pallet");

                entity.Property(e => e.PartpalletId).HasColumnName("partpallet_id");

                entity.Property(e => e.BestBeforeDate)
                    .HasColumnName("best_before_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.IsEol)
                    .IsRequired()
                    .HasColumnName("is_eol")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.NumCases)
                    .HasColumnName("num_cases")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.PalletNumber)
                    .HasColumnName("pallet_number")
                    .HasMaxLength(50);

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecUser).HasColumnName("rec_user");
            });

            modelBuilder.Entity<CcaPartPalletPrinter>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_part_pallet_printer");

                entity.HasIndex(e => e.PartpalletId)
                    .HasName("IX_cca_part_pallet_printer");

                entity.HasIndex(e => e.PrinterId)
                    .HasName("IX_cca_part_pallet_printer_1");

                entity.HasIndex(e => new { e.PartpalletId, e.PrinterId })
                    .HasName("IX_cca_part_pallet_printer_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.PartpalletId).HasColumnName("partpallet_id");

                entity.Property(e => e.PrinterId).HasColumnName("printer_id");
            });

            modelBuilder.Entity<CcaPlant>(entity =>
            {
                entity.HasKey(e => e.PlantId)
                    .HasName("PK__cca_plan__A576B3B4AE4918B4");

                entity.ToTable("cca_plant");

                entity.HasIndex(e => e.PlantCode)
                    .HasName("IX_cca_plant");

                entity.Property(e => e.PlantId).HasColumnName("plant_id");

                entity.Property(e => e.PlantCode)
                    .IsRequired()
                    .HasColumnName("plant_code")
                    .HasMaxLength(10);

                entity.Property(e => e.PlantDescr)
                    .IsRequired()
                    .HasColumnName("plant_descr")
                    .HasMaxLength(50);

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<CcaPlantJoin>(entity =>
            {
                entity.HasNoKey();
            });

            modelBuilder.Entity<CcaPlantEwm>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_plant_ewm");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.PlantId).HasColumnName("plant_id");

                entity.Property(e => e.UseEwm)
                    .IsRequired()
                    .HasColumnName("use_ewm")
                    .HasMaxLength(1)
                    .IsFixedLength();
            });

            modelBuilder.Entity<CcaPlantLine>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_plant_line");

                entity.HasIndex(e => e.LineId)
                    .HasName("IX_cca_plant_line_1");

                entity.HasIndex(e => e.PlantId)
                    .HasName("IX_cca_plant_line");

                entity.HasIndex(e => new { e.PlantId, e.LineId })
                    .HasName("IX_cca_plant_line_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.LineId).HasColumnName("line_id");

                entity.Property(e => e.PlantId).HasColumnName("plant_id");
            });

            modelBuilder.Entity<CcaPlantRealtime>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_plant_realtime");

                entity.HasIndex(e => e.PlantId)
                    .HasName("IX_cca_plant_realtime");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.GrInterval).HasColumnName("gr_interval");

                entity.Property(e => e.IsInterval)
                    .IsRequired()
                    .HasColumnName("is_interval")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.PlantId).HasColumnName("plant_id");
            });

            modelBuilder.Entity<CcaPlantWms>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_plant_wms");

                entity.HasIndex(e => e.PlantId)
                    .HasName("IX_cca_plant_wms");

                entity.HasIndex(e => e.UseWms)
                    .HasName("IX_cca_plant_wms_1");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.PlantId).HasColumnName("plant_id");

                entity.Property(e => e.UseWms)
                    .IsRequired()
                    .HasColumnName("use_wms")
                    .HasMaxLength(1)
                    .IsFixedLength();
            });

            modelBuilder.Entity<CcaPrinter>(entity =>
            {
                entity.HasKey(e => e.PrinterId)
                    .HasName("PK__cca_prin__057FF71626EA14C6");

                entity.ToTable("cca_printer");

                entity.Property(e => e.PrinterId).HasColumnName("printer_id");

                entity.Property(e => e.IsOnline)
                    .IsRequired()
                    .HasColumnName("is_online")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.PrinterDescr)
                    .IsRequired()
                    .HasColumnName("printer_descr")
                    .HasColumnType("ntext");

                entity.Property(e => e.PrinterIp)
                    .IsRequired()
                    .HasColumnName("printer_ip")
                    .HasMaxLength(50);

                entity.Property(e => e.PrinterName)
                    .IsRequired()
                    .HasColumnName("printer_name")
                    .HasMaxLength(50);

                entity.Property(e => e.PrinterPort).HasColumnName("printer_port");

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<CcaPrinterPlant>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_printer_plant");

                entity.HasIndex(e => e.PlantId)
                    .HasName("IX_cca_printer_plant_1");

                entity.HasIndex(e => e.PrinterId)
                    .HasName("IX_cca_printer_plant");

                entity.HasIndex(e => new { e.PrinterId, e.PlantId })
                    .HasName("IX_cca_printer_plant_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.PlantId).HasColumnName("plant_id");

                entity.Property(e => e.PrinterId).HasColumnName("printer_id");
            });

            modelBuilder.Entity<CcaPrintreason>(entity =>
            {
                entity.HasKey(e => e.PrintreasonId)
                    .HasName("PK__cca_prin__2F87293F625A2A70");

                entity.ToTable("cca_printreason");

                entity.Property(e => e.PrintreasonId).HasColumnName("printreason_id");

                entity.Property(e => e.PrintreasonCode)
                    .IsRequired()
                    .HasColumnName("printreason_code")
                    .HasMaxLength(10);

                entity.Property(e => e.PrintreasonName)
                    .IsRequired()
                    .HasColumnName("printreason_name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<CcaRolePlant>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_role_plant");

                entity.HasIndex(e => e.PlantId)
                    .HasName("IX_cca_role_plant_1");

                entity.HasIndex(e => e.RoleId)
                    .HasName("IX_cca_role_plant");

                entity.HasIndex(e => new { e.PlantId, e.RoleId })
                    .HasName("IX_cca_role_plant_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.PlantId).HasColumnName("plant_id");

                entity.Property(e => e.RoleId).HasColumnName("role_id");
            });

            modelBuilder.Entity<CcaScanner>(entity =>
            {
                entity.HasKey(e => e.ScannerId)
                    .HasName("PK__cca_scan__FAE28767AFF8B122");

                entity.ToTable("cca_scanner");

                entity.Property(e => e.ScannerId).HasColumnName("scanner_id");

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ScannerIp)
                    .IsRequired()
                    .HasColumnName("scanner_ip")
                    .HasMaxLength(50);

                entity.Property(e => e.ScannerName)
                    .IsRequired()
                    .HasColumnName("scanner_name")
                    .HasMaxLength(50);

                entity.Property(e => e.ScannerPort).HasColumnName("scanner_port");
            });

            modelBuilder.Entity<CcaScannerPlant>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_scanner_plant");

                entity.HasIndex(e => e.PlantId)
                    .HasName("IX_cca_scanner_plant_1");

                entity.HasIndex(e => e.ScannerId)
                    .HasName("IX_cca_scanner_plant");

                entity.HasIndex(e => new { e.PlantId, e.ScannerId })
                    .HasName("IX_cca_scanner_plant_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.PlantId).HasColumnName("plant_id");

                entity.Property(e => e.ScannerId).HasColumnName("scanner_id");
            });

            modelBuilder.Entity<CcaService>(entity =>
            {
                entity.HasKey(e => e.ServiceId)
                    .HasName("PK__cca_serv__3E0DB8AFDDF8166C");

                entity.ToTable("cca_service");

                entity.Property(e => e.ServiceId)
                    .HasColumnName("service_id")
                    .ValueGeneratedNever();

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ServiceDescr)
                    .IsRequired()
                    .HasColumnName("service_descr")
                    .HasColumnType("text");

                entity.Property(e => e.ServiceName)
                    .IsRequired()
                    .HasColumnName("service_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CcaServiceLine>(entity =>
            {
                entity.HasKey(e => e.Seqid)
                    .HasName("PK__cca_serv__DE13A1F35C8A5501");

                entity.ToTable("cca_service_line");

                entity.HasIndex(e => e.LineId)
                    .HasName("IX_cca_service_line");

                entity.HasIndex(e => e.ServiceId)
                    .HasName("IX_cca_service_line_1");

                entity.HasIndex(e => new { e.LineId, e.ServiceId })
                    .HasName("IX_cca_service_line_2");

                entity.Property(e => e.Seqid)
                    .HasColumnName("seqid")
                    .ValueGeneratedNever();

                entity.Property(e => e.LineId).HasColumnName("line_id");

                entity.Property(e => e.ServiceId).HasColumnName("service_id");
            });

            modelBuilder.Entity<CcaServiceStatus>(entity =>
            {
                entity.HasKey(e => e.Seqid)
                    .HasName("PK__cca_serv__DE13A1F33C0A0567");

                entity.ToTable("cca_service_status");

                entity.HasIndex(e => e.ServiceId)
                    .HasName("IX_cca_service_status");

                entity.Property(e => e.Seqid)
                    .HasColumnName("seqid")
                    .ValueGeneratedNever();

                entity.Property(e => e.ServiceId).HasColumnName("service_id");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("status")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<CcaSettings>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_settings");

                entity.HasIndex(e => e.Option1)
                    .HasName("IX_cca_settings");

                entity.HasIndex(e => e.Option2)
                    .HasName("IX_cca_settings_1");

                entity.HasIndex(e => new { e.Option1, e.Option2 })
                    .HasName("IX_cca_settings_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.Option1)
                    .IsRequired()
                    .HasColumnName("option_1")
                    .HasMaxLength(100);

                entity.Property(e => e.Option2)
                    .IsRequired()
                    .HasColumnName("option_2")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<CcaSscc>(entity =>
            {
                entity.HasKey(e => e.SsccId)
                    .HasName("PK__cca_sscc__99B8E01DCF657D8B");

                entity.ToTable("cca_sscc");

                entity.HasIndex(e => e.SsccNumber)
                    .HasName("IX_cca_sscc");

                entity.Property(e => e.SsccId).HasColumnName("sscc_id");

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsccNumber)
                    .IsRequired()
                    .HasColumnName("sscc_number")
                    .HasMaxLength(20);

                entity.Property(e => e.SsccQty)
                    .HasColumnName("sscc_qty")
                    .HasColumnType("numeric(18, 0)");
            });

            modelBuilder.Entity<CcaSsccBlockstock>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_sscc_blockstock");

                entity.HasIndex(e => e.IsBlockstock)
                    .HasName("IX_cca_sscc_blockstock_1");

                entity.HasIndex(e => e.SsccId)
                    .HasName("IX_cca_sscc_blockstock");

                entity.HasIndex(e => new { e.SsccId, e.IsBlockstock })
                    .HasName("IX_cca_sscc_blockstock_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.IsBlockstock)
                    .IsRequired()
                    .HasColumnName("is_blockstock")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsccId).HasColumnName("sscc_id");
            });

            modelBuilder.Entity<CcaSsccContent>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_sscc_content");

                entity.HasIndex(e => e.BarcodeToken)
                    .HasName("IX_cca_sscc_content_1");

                entity.HasIndex(e => e.PalletNumber)
                    .HasName("IX_cca_sscc_content_3");

                entity.HasIndex(e => e.SsccId)
                    .HasName("IX_cca_sscc_content");

                entity.HasIndex(e => e.SsccNumber)
                    .HasName("IX_cca_sscc_content_2");

                entity.HasIndex(e => new { e.SsccId, e.PalletNumber })
                    .HasName("IX_cca_sscc_content_4");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.ArticleName)
                    .IsRequired()
                    .HasColumnName("article_name")
                    .HasMaxLength(50);

                entity.Property(e => e.ArticleQty)
                    .HasColumnName("article_qty")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.BarcodeToken)
                    .IsRequired()
                    .HasColumnName("barcode_token")
                    .HasMaxLength(200);

                entity.Property(e => e.BatchNo)
                    .IsRequired()
                    .HasColumnName("batch_no")
                    .HasMaxLength(20);

                entity.Property(e => e.BestBeforeDate)
                    .HasColumnName("best_before_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ItemMaterialNo)
                    .IsRequired()
                    .HasColumnName("item_material_no")
                    .HasMaxLength(10);

                entity.Property(e => e.PalletNumber)
                    .IsRequired()
                    .HasColumnName("pallet_number")
                    .HasMaxLength(20);

                entity.Property(e => e.PlantName)
                    .IsRequired()
                    .HasColumnName("plant_name")
                    .HasColumnType("ntext");

                entity.Property(e => e.ProdCode)
                    .IsRequired()
                    .HasColumnName("prod_code")
                    .HasMaxLength(50);

                entity.Property(e => e.ProdDate)
                    .HasColumnName("prod_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.SsccId).HasColumnName("sscc_id");

                entity.Property(e => e.SsccNumber)
                    .IsRequired()
                    .HasColumnName("sscc_number")
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<CcaSsccDevice>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_sscc_device");

                entity.HasIndex(e => e.DeviceId)
                    .HasName("IX_cca_sscc_device_1");

                entity.HasIndex(e => e.SsccId)
                    .HasName("IX_cca_sscc_device");

                entity.HasIndex(e => new { e.SsccId, e.DeviceId })
                    .HasName("IX_cca_sscc_device_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.DeviceId).HasColumnName("device_id");

                entity.Property(e => e.DeviceType)
                    .IsRequired()
                    .HasColumnName("device_type")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.SsccId).HasColumnName("sscc_id");
            });

            modelBuilder.Entity<CcaSsccExtraLabel>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_sscc_extra_label");

                entity.HasIndex(e => e.ExtralabelId)
                    .HasName("IX_cca_sscc_extra_label_1");

                entity.HasIndex(e => e.SsccId)
                    .HasName("IX_cca_sscc_extra_label");

                entity.HasIndex(e => new { e.SsccId, e.ExtralabelId })
                    .HasName("IX_cca_sscc_extra_label_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.ExtralabelId).HasColumnName("extralabel_id");

                entity.Property(e => e.SsccId).HasColumnName("sscc_id");
            });

            modelBuilder.Entity<CcaSsccFlag>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_sscc_flag");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.Count).HasColumnName("count");

                entity.Property(e => e.Flag)
                    .HasColumnName("flag")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.LineId).HasColumnName("line_id");

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<CcaSsccMobile>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_sscc_mobile");

                entity.HasIndex(e => e.IsMobile)
                    .HasName("IX_cca_sscc_mobile_1");

                entity.HasIndex(e => e.RecUser)
                    .HasName("IX_cca_sscc_mobile_2");

                entity.HasIndex(e => e.SsccId)
                    .HasName("IX_cca_sscc_mobile");

                entity.HasIndex(e => new { e.SsccId, e.IsMobile })
                    .HasName("IX_cca_sscc_mobile_3");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.IsMobile)
                    .IsRequired()
                    .HasColumnName("is_mobile")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecUser).HasColumnName("rec_user");

                entity.Property(e => e.SsccId).HasColumnName("sscc_id");
            });

            modelBuilder.Entity<CcaSsccPrint>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_sscc_print");

                entity.HasIndex(e => e.RecUser)
                    .HasName("IX_cca_sscc_print_1");

                entity.HasIndex(e => e.SsccId)
                    .HasName("IX_cca_sscc_print");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.IsPrinted)
                    .IsRequired()
                    .HasColumnName("is_printed")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecUser).HasColumnName("rec_user");

                entity.Property(e => e.SsccId).HasColumnName("sscc_id");
            });

            modelBuilder.Entity<CcaSsccPrinter>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_sscc_printer");

                entity.HasIndex(e => e.IsManual)
                    .HasName("IX_cca_sscc_printer_1");

                entity.HasIndex(e => e.SsccId)
                    .HasName("IX_cca_sscc_printer");

                entity.HasIndex(e => new { e.SsccId, e.IsManual })
                    .HasName("IX_cca_sscc_printer_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.IsManual)
                    .IsRequired()
                    .HasColumnName("is_manual")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.PrinterName)
                    .HasColumnName("printer_name")
                    .HasColumnType("ntext");

                entity.Property(e => e.SsccId).HasColumnName("sscc_id");
            });

            modelBuilder.Entity<CcaSsccPrintreason>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_sscc_printreason");

                entity.HasIndex(e => e.PrintreasonId)
                    .HasName("IX_cca_sscc_printreason_1");

                entity.HasIndex(e => e.SsccId)
                    .HasName("IX_cca_sscc_printreason");

                entity.HasIndex(e => new { e.SsccId, e.PrintreasonId })
                    .HasName("IX_cca_sscc_printreason_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.PrintreasonId).HasColumnName("printreason_id");

                entity.Property(e => e.SsccId).HasColumnName("sscc_id");
            });

            modelBuilder.Entity<CcaSsccQueue>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_sscc_queue");

                entity.HasIndex(e => e.BarcodeToken)
                    .HasName("IX_cca_sscc_queue_1");

                entity.HasIndex(e => e.SsccId)
                    .HasName("IX_cca_sscc_queue");

                entity.HasIndex(e => new { e.SsccId, e.BarcodeToken })
                    .HasName("IX_cca_sscc_queue_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.BarcodeToken)
                    .IsRequired()
                    .HasColumnName("barcode_token")
                    .HasMaxLength(200);

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsccId).HasColumnName("sscc_id");
            });

            modelBuilder.Entity<CcaSsccReprinted>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_sscc_reprinted");

                entity.HasIndex(e => e.RecUser)
                    .HasName("IX_cca_sscc_reprinted_1");

                entity.HasIndex(e => e.SsccId)
                    .HasName("IX_cca_sscc_reprinted");

                entity.HasIndex(e => new { e.SsccId, e.IsReprinted })
                    .HasName("IX_cca_sscc_reprinted_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.IsReprinted)
                    .IsRequired()
                    .HasColumnName("is_reprinted")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecUser).HasColumnName("rec_user");

                entity.Property(e => e.SsccId).HasColumnName("sscc_id");
            });

            modelBuilder.Entity<CcaSsccScan>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_sscc_scan");

                entity.HasIndex(e => e.PalletNumber)
                    .HasName("IX_cca_sscc_scan_2");

                entity.HasIndex(e => e.RecUser)
                    .HasName("IX_cca_sscc_scan_1");

                entity.HasIndex(e => e.SsccId)
                    .HasName("IX_cca_sscc_scan");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.PalletNumber)
                    .IsRequired()
                    .HasColumnName("pallet_number")
                    .HasMaxLength(20);

                entity.Property(e => e.ProdDatetime)
                    .HasColumnName("prod_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecUser).HasColumnName("rec_user");

                entity.Property(e => e.SsccId).HasColumnName("sscc_id");
            });

            modelBuilder.Entity<CcaSsccScrapped>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_sscc_scrapped");

                entity.HasIndex(e => e.SsccId)
                    .HasName("IX_cca_sscc_scrapped");

                entity.HasIndex(e => e.SsccNumber)
                    .HasName("IX_cca_sscc_scrapped_1");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsccId).HasColumnName("sscc_id");

                entity.Property(e => e.SsccNumber)
                    .IsRequired()
                    .HasColumnName("sscc_number")
                    .HasMaxLength(20);

                entity.Property(e => e.SsccQty)
                    .HasColumnName("sscc_qty")
                    .HasColumnType("numeric(18, 0)");
            });

            modelBuilder.Entity<CcaSsccStart>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_sscc_start");

                entity.HasIndex(e => e.RecUser)
                    .HasName("IX_cca_sscc_start_1");

                entity.HasIndex(e => e.SsccId)
                    .HasName("IX_cca_sscc_start");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.IsStart)
                    .IsRequired()
                    .HasColumnName("is_start")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecUser).HasColumnName("rec_user");

                entity.Property(e => e.SsccId).HasColumnName("sscc_id");
            });

            modelBuilder.Entity<CcaSsccSync>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_sscc_sync");

                entity.HasIndex(e => e.IsSynced)
                    .HasName("IX_cca_sscc_sync_1");

                entity.HasIndex(e => e.SsccId)
                    .HasName("IX_cca_sscc_sync");

                entity.HasIndex(e => new { e.SsccId, e.IsSynced })
                    .HasName("IX_cca_sscc_sync_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.IsSynced).HasColumnName("is_synced");

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsccId).HasColumnName("sscc_id");
            });

            modelBuilder.Entity<CcaWorkorder>(entity =>
            {
                entity.HasKey(e => e.WorkorderId)
                    .HasName("PK__cca_work__A4128E86DC3183C7");

                entity.ToTable("cca_workorder");

                entity.HasIndex(e => e.ItemKey)
                    .HasName("IX_cca_workorder_1");

                entity.HasIndex(e => e.WorkorderNo)
                    .HasName("IX_cca_workorder");

                entity.Property(e => e.WorkorderId).HasColumnName("workorder_id");

                entity.Property(e => e.BatchNo)
                    .IsRequired()
                    .HasColumnName("batch_no")
                    .HasMaxLength(50);

                entity.Property(e => e.ExpectedCase)
                    .HasColumnName("expected_case")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.ExpectedPallets)
                    .HasColumnName("expected_pallets")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.ExpectedProdDate)
                    .HasColumnName("expected_prod_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ItemKey).HasColumnName("item_key");

                entity.Property(e => e.ItemQty)
                    .HasColumnName("item_qty")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.ProdDate)
                    .HasColumnName("prod_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.WorkorderNo)
                    .IsRequired()
                    .HasColumnName("workorder_no")
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<CcaWorkorderEol>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_workorder_eol");

                entity.HasIndex(e => e.WorkorderId)
                    .HasName("IX_cca_workorder_eol");

                entity.HasIndex(e => new { e.WorkorderId, e.IsEol })
                    .HasName("IX_cca_workorder_eol_1");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.IsEol)
                    .IsRequired()
                    .HasColumnName("is_eol")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.WorkorderId).HasColumnName("workorder_id");
            });

            modelBuilder.Entity<CcaWorkorderExtraLabel>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_workorder_extra_label");

                entity.HasIndex(e => e.ExtralabelId)
                    .HasName("IX_cca_workorder_extra_label_1");

                entity.HasIndex(e => e.WorkorderId)
                    .HasName("IX_cca_workorder_extra_label");

                entity.HasIndex(e => new { e.WorkorderId, e.ExtralabelId })
                    .HasName("IX_cca_workorder_extra_label_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.ExtralabelId).HasColumnName("extralabel_id");

                entity.Property(e => e.WorkorderId).HasColumnName("workorder_id");
            });

            modelBuilder.Entity<CcaWorkorderLine>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_workorder_line");

                entity.HasIndex(e => e.LineId)
                    .HasName("IX_cca_workorder_line_1");

                entity.HasIndex(e => e.WorkorderId)
                    .HasName("IX_cca_workorder_line");

                entity.HasIndex(e => new { e.LineId, e.WorkorderId })
                    .HasName("IX_cca_workorder_line_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.LineId).HasColumnName("line_id");

                entity.Property(e => e.WorkorderId).HasColumnName("workorder_id");
            });

            modelBuilder.Entity<CcaWorkorderLineCurrent>(entity =>
            {
                entity.HasKey(e => e.Seqid)
                    .HasName("PK_cca_workorder_line_current_2");

                entity.ToTable("cca_workorder_line_current");

                entity.HasIndex(e => e.LineId)
                    .HasName("IX_cca_workorder_line_current_2");

                entity.HasIndex(e => e.PlantId)
                    .HasName("IX_cca_workorder_line_current_1");

                entity.HasIndex(e => e.RecUser)
                    .HasName("IX_cca_workorder_line_current_3");

                entity.HasIndex(e => e.WorkorderId)
                    .HasName("IX_cca_workorder_line_current");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.LineId).HasColumnName("line_id");

                entity.Property(e => e.PlantId).HasColumnName("plant_id");

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecUser)
                    .HasColumnName("rec_user")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WorkorderId).HasColumnName("workorder_id");
            });

            modelBuilder.Entity<CcaWorkorderMedia>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_workorder_media");

                entity.HasIndex(e => e.MediaId)
                    .HasName("IX_cca_workorder_media_1");

                entity.HasIndex(e => e.WorkorderId)
                    .HasName("IX_cca_workorder_media");

                entity.HasIndex(e => new { e.WorkorderId, e.MediaId })
                    .HasName("IX_cca_workorder_media_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.MediaId).HasColumnName("media_id");

                entity.Property(e => e.WorkorderId).HasColumnName("workorder_id");
            });

            modelBuilder.Entity<CcaWorkorderOrderType>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_workorder_order_type");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.Ordertype)
                    .IsRequired()
                    .HasColumnName("ordertype")
                    .HasMaxLength(10);

                entity.Property(e => e.WorkorderId).HasColumnName("workorder_id");
            });

            modelBuilder.Entity<CcaWorkorderPalletType>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("cca_workorder_pallet_type");

                entity.Property(e => e.IsScanned)
                    .HasColumnName("is_scanned")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PalletType)
                    .HasColumnName("pallet_type")
                    .HasMaxLength(5);

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.SsccId).HasColumnName("sscc_id");

                entity.Property(e => e.WorkorderId).HasColumnName("workorder_id");
            });

            modelBuilder.Entity<CcaWorkorderPalletTypeLog>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_workorder_pallet_type_log");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.PalletType)
                    .HasColumnName("pallet_type")
                    .HasMaxLength(5);

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.SsccId).HasColumnName("sscc_id");

                entity.Property(e => e.WorkorderId).HasColumnName("workorder_id");
            });

            modelBuilder.Entity<CcaWorkorderPartPallet>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_workorder_part_pallet");

                entity.HasIndex(e => e.PartpalletId)
                    .HasName("IX_cca_workorder_part_pallet_1");

                entity.HasIndex(e => e.WorkorderId)
                    .HasName("IX_cca_workorder_part_pallet");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.PartpalletId).HasColumnName("partpallet_id");

                entity.Property(e => e.WorkorderId).HasColumnName("workorder_id");
            });

            modelBuilder.Entity<CcaWorkorderPlant>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_workorder_plant");

                entity.HasIndex(e => e.PlantId)
                    .HasName("IX_cca_workorder_plant_1");

                entity.HasIndex(e => e.WorkorderId)
                    .HasName("IX_cca_workorder_plant");

                entity.HasIndex(e => new { e.PlantId, e.WorkorderId })
                    .HasName("IX_cca_workorder_plant_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.PlantId).HasColumnName("plant_id");

                entity.Property(e => e.WorkorderId).HasColumnName("workorder_id");
            });

            modelBuilder.Entity<CcaWorkorderRegister>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_workorder_register");

                entity.HasIndex(e => e.RecUser)
                    .HasName("IX_cca_workorder_register_1");

                entity.HasIndex(e => e.WorkorderId)
                    .HasName("IX_cca_workorder_register");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.CurrentPallet).HasColumnName("current_pallet");

                entity.Property(e => e.EndPallet).HasColumnName("end_pallet");

                entity.Property(e => e.LastPallet).HasColumnName("last_pallet");

                entity.Property(e => e.NumPallet).HasColumnName("num_pallet");

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecUser).HasColumnName("rec_user");

                entity.Property(e => e.StartPallet).HasColumnName("start_pallet");

                entity.Property(e => e.WorkorderId).HasColumnName("workorder_id");
            });

            modelBuilder.Entity<CcaWorkorderRegisterHistory>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_workorder_register_history");

                entity.HasIndex(e => e.WorkorderId)
                    .HasName("IX_cca_workorder_register_history");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.CurrentPallet).HasColumnName("current_pallet");

                entity.Property(e => e.EndPallet).HasColumnName("end_pallet");

                entity.Property(e => e.LastPallet).HasColumnName("last_pallet");

                entity.Property(e => e.NumPallet).HasColumnName("num_pallet");

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecUser).HasColumnName("rec_user");

                entity.Property(e => e.StartPallet).HasColumnName("start_pallet");

                entity.Property(e => e.WorkorderId).HasColumnName("workorder_id");
            });

            modelBuilder.Entity<CcaWorkorderSscc>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_workorder_sscc");

                entity.HasIndex(e => e.SsccId)
                    .HasName("IX_cca_workorder_sscc_1");

                entity.HasIndex(e => e.WorkorderId)
                    .HasName("IX_cca_workorder_sscc");

                entity.HasIndex(e => new { e.WorkorderId, e.SsccId })
                    .HasName("IX_cca_workorder_sscc_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.SsccId).HasColumnName("sscc_id");

                entity.Property(e => e.WorkorderId).HasColumnName("workorder_id");
            });

            modelBuilder.Entity<CcaWorkorderStatus>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("cca_workorder_status");

                entity.HasIndex(e => e.WorkorderId)
                    .HasName("IX_cca_workorder_status");

                entity.HasIndex(e => new { e.WorkorderId, e.Status })
                    .HasName("IX_cca_workorder_status_1");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("status")
                    .HasMaxLength(10);

                entity.Property(e => e.WorkorderId).HasColumnName("workorder_id");
            });

            modelBuilder.Entity<MasterAdmin>(entity =>
            {
                entity.HasKey(e => e.AdminId)
                    .HasName("PK__master_a__43AA41419E41BB9A");

                entity.ToTable("master_admin");

                entity.HasIndex(e => e.AdminName)
                    .HasName("IX_master_admin");

                entity.HasIndex(e => new { e.AdminId, e.IsActive })
                    .HasName("IX_master_admin_1");

                entity.Property(e => e.AdminId).HasColumnName("admin_id");

                entity.Property(e => e.AdminEmail)
                    .IsRequired()
                    .HasColumnName("admin_email")
                    .HasMaxLength(50);

                entity.Property(e => e.AdminFullname)
                    .IsRequired()
                    .HasColumnName("admin_fullname")
                    .HasMaxLength(50);

                entity.Property(e => e.AdminName)
                    .IsRequired()
                    .HasColumnName("admin_name")
                    .HasMaxLength(50);

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasColumnName("is_active")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<MasterAdminCredential>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("master_admin_credential");

                entity.HasIndex(e => e.AdminId)
                    .HasName("IX_master_admin_credential");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.AdminId).HasColumnName("admin_id");

                entity.Property(e => e.AdminPass)
                    .IsRequired()
                    .HasColumnName("admin_pass")
                    .HasMaxLength(200);

                entity.Property(e => e.AdminSalt)
                    .IsRequired()
                    .HasColumnName("admin_salt")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<MasterRole>(entity =>
            {
                entity.HasKey(e => e.RoleId)
                    .HasName("PK__master_r__760965CCF71DF302");

                entity.ToTable("master_role");

                entity.HasIndex(e => e.RoleName)
                    .HasName("IX_master_role");

                entity.Property(e => e.RoleId).HasColumnName("role_id");

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasColumnName("role_name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<MasterRoleAdmin>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("master_role_admin");

                entity.HasIndex(e => e.IsAdmin)
                    .HasName("IX_master_role_admin");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.IsAdmin)
                    .IsRequired()
                    .HasColumnName("is_admin")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.RoleId).HasColumnName("role_id");
            });

            modelBuilder.Entity<MasterRolePlant>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("master_role_plant");

                entity.HasIndex(e => e.PlantId)
                    .HasName("IX_master_role_plant_1");

                entity.HasIndex(e => e.RoleId)
                    .HasName("IX_master_role_plant");

                entity.HasIndex(e => new { e.PlantId, e.RoleId })
                    .HasName("IX_master_role_plant_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.PlantId).HasColumnName("plant_id");

                entity.Property(e => e.RoleId).HasColumnName("role_id");
            });

            modelBuilder.Entity<MasterSecurity>(entity =>
            {
                entity.HasKey(e => e.SecurityId)
                    .HasName("PK__master_s__46647BD1AA21FF64");

                entity.ToTable("master_security");

                entity.HasIndex(e => e.SecurityCode)
                    .HasName("IX_master_security");

                entity.HasIndex(e => e.Seq)
                    .HasName("IX_master_security_1");

                entity.Property(e => e.SecurityId).HasColumnName("security_id");

                entity.Property(e => e.SecurityCode)
                    .HasColumnName("security_code")
                    .HasMaxLength(10);

                entity.Property(e => e.SecurityDescr)
                    .IsRequired()
                    .HasColumnName("security_descr")
                    .HasColumnType("ntext");

                entity.Property(e => e.SecurityName)
                    .IsRequired()
                    .HasColumnName("security_name")
                    .HasMaxLength(100);

                entity.Property(e => e.Seq).HasColumnName("seq");
            });

            modelBuilder.Entity<MasterSecurityRole>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("master_security_role");

                entity.HasIndex(e => e.Nread)
                    .HasName("IX_master_security_role_2");

                entity.HasIndex(e => e.Nwrite)
                    .HasName("IX_master_security_role_3");

                entity.HasIndex(e => e.RoleId)
                    .HasName("IX_master_security_role_1");

                entity.HasIndex(e => e.SecurityId)
                    .HasName("IX_master_security_role");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.Nread)
                    .IsRequired()
                    .HasColumnName("nread")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.Nwrite)
                    .IsRequired()
                    .HasColumnName("nwrite")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.RoleId).HasColumnName("role_id");

                entity.Property(e => e.SecurityId).HasColumnName("security_id");
            });

            modelBuilder.Entity<MasterSessions>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("master_sessions");

                entity.HasIndex(e => e.IpAddress)
                    .HasName("IX_master_sessions_1");

                entity.HasIndex(e => e.SessionId)
                    .HasName("IX_master_sessions");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.IpAddress)
                    .IsRequired()
                    .HasColumnName("ip_address")
                    .HasMaxLength(50);

                entity.Property(e => e.LastActivity).HasColumnName("last_activity");

                entity.Property(e => e.SessionId)
                    .IsRequired()
                    .HasColumnName("session_id")
                    .HasMaxLength(40);

                entity.Property(e => e.UserAgent)
                    .IsRequired()
                    .HasColumnName("user_agent")
                    .HasMaxLength(200);

                entity.Property(e => e.UserData)
                    .IsRequired()
                    .HasColumnName("user_data")
                    .HasColumnType("text");
            });

            modelBuilder.Entity<MasterUser>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PK__master_u__B9BE370FBC5D3345");

                entity.ToTable("master_user");

                entity.HasIndex(e => e.UserName)
                    .HasName("IX_master_user");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.RecCreated)
                    .HasColumnName("rec_created")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecDatetime)
                    .HasColumnName("rec_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.UserEmail)
                    .IsRequired()
                    .HasColumnName("user_email")
                    .HasMaxLength(50);

                entity.Property(e => e.UserFullname)
                    .IsRequired()
                    .HasColumnName("user_fullname")
                    .HasMaxLength(50);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnName("user_name")
                    .HasMaxLength(50);

                entity.Property(e => e.UserPhone)
                    .IsRequired()
                    .HasColumnName("user_phone")
                    .HasMaxLength(50);

                entity.Property(e => e.UserStatus)
                    .IsRequired()
                    .HasColumnName("user_status")
                    .HasMaxLength(1)
                    .IsFixedLength();
            });

            modelBuilder.Entity<MasterUserCredential>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("master_user_credential");

                entity.HasIndex(e => e.UserId)
                    .HasName("IX_master_user_credential");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.UserPass)
                    .IsRequired()
                    .HasColumnName("user_pass")
                    .HasMaxLength(200);

                entity.Property(e => e.UserSalt)
                    .IsRequired()
                    .HasColumnName("user_salt")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<MasterUserLanguage>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("master_user_language");

                entity.HasIndex(e => e.Language)
                    .HasName("IX_master_user_language_1");

                entity.HasIndex(e => e.UserId)
                    .HasName("IX_master_user_language");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.Language)
                    .IsRequired()
                    .HasColumnName("language")
                    .HasMaxLength(5);

                entity.Property(e => e.UserId).HasColumnName("user_id");
            });

            modelBuilder.Entity<MasterUserPlant>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("master_user_plant");

                entity.HasIndex(e => e.PlantId)
                    .HasName("IX_master_user_plant_1");

                entity.HasIndex(e => e.UserId)
                    .HasName("IX_master_user_plant");

                entity.HasIndex(e => new { e.PlantId, e.UserId })
                    .HasName("IX_master_user_plant_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.PlantId).HasColumnName("plant_id");

                entity.Property(e => e.UserId).HasColumnName("user_id");
            });

            modelBuilder.Entity<MasterUserRole>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("master_user_role");

                entity.HasIndex(e => e.RoleId)
                    .HasName("IX_master_user_role_1");

                entity.HasIndex(e => e.UserId)
                    .HasName("IX_master_user_role");

                entity.HasIndex(e => new { e.RoleId, e.UserId })
                    .HasName("IX_master_user_role_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.RoleId).HasColumnName("role_id");

                entity.Property(e => e.UserId).HasColumnName("user_id");
            });

            modelBuilder.Entity<MasterUserSelectPlant>(entity =>
            {
                entity.HasKey(e => e.Seqid);

                entity.ToTable("master_user_select_plant");

                entity.HasIndex(e => e.PlantId)
                    .HasName("IX_master_user_select_plant_1");

                entity.HasIndex(e => e.UserId)
                    .HasName("IX_master_user_select_plant");

                entity.HasIndex(e => new { e.PlantId, e.UserId })
                    .HasName("IX_master_user_select_plant_2");

                entity.Property(e => e.Seqid).HasColumnName("seqid");

                entity.Property(e => e.PlantId).HasColumnName("plant_id");

                entity.Property(e => e.UserId).HasColumnName("user_id");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
