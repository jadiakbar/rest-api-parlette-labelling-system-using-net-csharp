﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaPrinter
    {
        public int PrinterId { get; set; }
        public string PrinterName { get; set; }
        public string PrinterDescr { get; set; }
        public string PrinterIp { get; set; }
        public int PrinterPort { get; set; }
        public string IsOnline { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
    }
}
