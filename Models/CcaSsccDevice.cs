﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaSsccDevice
    {
        public long SsccId { get; set; }
        public int DeviceId { get; set; }
        public string DeviceType { get; set; }
        public long Seqid { get; set; }
    }
}
