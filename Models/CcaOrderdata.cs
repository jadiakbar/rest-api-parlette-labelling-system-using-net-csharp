﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaOrderdata
    {
        public long OrderdataId { get; set; }
        public string OrderHeader { get; set; }
        public string OrderPlant { get; set; }
        public string OrderLine { get; set; }
        public string OrderProcess { get; set; }
        public string OrderMaterial { get; set; }
        public DateTime OrderProddate { get; set; }
        public decimal OrderQtyorder { get; set; }
        public string OrderEnder { get; set; }
        public DateTime RecCreated { get; set; }
        public DateTime RecDatetime { get; set; }
    }
}
