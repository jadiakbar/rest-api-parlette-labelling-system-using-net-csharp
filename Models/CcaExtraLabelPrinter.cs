﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaExtraLabelPrinter
    {
        public long ExtralabelId { get; set; }
        public int PrinterId { get; set; }
        public int Seqid { get; set; }
    }
}
