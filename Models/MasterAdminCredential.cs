﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class MasterAdminCredential
    {
        public int AdminId { get; set; }
        public string AdminPass { get; set; }
        public string AdminSalt { get; set; }
        public int Seqid { get; set; }
    }
}
