﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaItemModby
    {
        public int Seqid { get; set; }
        public int? ItemKey { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? RecCreated { get; set; }
    }
}
