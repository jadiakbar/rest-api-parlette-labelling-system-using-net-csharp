﻿using System;
using System.Collections.Generic;

namespace PlsAPI.Models
{
    public partial class CcaWorkorderSscc
    {
        public long WorkorderId { get; set; }
        public long SsccId { get; set; }
        public long Seqid { get; set; }
    }
}
