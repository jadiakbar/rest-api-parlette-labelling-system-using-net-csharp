﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlsAPI.Models;

namespace PlsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CcaLinePrintersController : ControllerBase
    {
        private readonly CCAIContext _context;

        public CcaLinePrintersController(CCAIContext context)
        {
            _context = context;
        }

        // GET: api/CcaLinePrinters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CcaLinePrinter>>> GetCcaLinePrinter()
        {
            return await _context.CcaLinePrinter.ToListAsync();
        }

        // GET: api/CcaLinePrinters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CcaLinePrinter>> GetCcaLinePrinter(long id)
        {
            var ccaLinePrinter = await _context.CcaLinePrinter.FindAsync(id);

            if (ccaLinePrinter == null)
            {
                return NotFound();
            }

            return ccaLinePrinter;
        }

        // PUT: api/CcaLinePrinters/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCcaLinePrinter(long id, CcaLinePrinter ccaLinePrinter)
        {
            if (id != ccaLinePrinter.Seqid)
            {
                return BadRequest();
            }

            _context.Entry(ccaLinePrinter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CcaLinePrinterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CcaLinePrinters
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<CcaLinePrinter>> PostCcaLinePrinter(CcaLinePrinter ccaLinePrinter)
        {
            _context.CcaLinePrinter.Add(ccaLinePrinter);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCcaLinePrinter", new { id = ccaLinePrinter.Seqid }, ccaLinePrinter);
        }

        // DELETE: api/CcaLinePrinters/5
        [HttpDelete("{id}")]
        /*public async Task<ActionResult<CcaLinePrinter>> DeleteCcaLinePrinter(long id)
        {
            var ccaLinePrinter = await _context.CcaLinePrinter.FindAsync(id);
            if (ccaLinePrinter == null)
            {
                return NotFound();
            }

            _context.CcaLinePrinter.Remove(ccaLinePrinter);
            await _context.SaveChangesAsync();

            return ccaLinePrinter;
        } */
        public async Task<ActionResult<CcaLinePrinter>> DeleteCcaLinePrinter(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cca_line_printer = await _context.CcaLinePrinter
            .FirstOrDefaultAsync(m => m.LineId == id);
            if (cca_line_printer == null)
            {
                return NotFound();
            }

            _context.CcaLinePrinter.Remove(cca_line_printer);
            await _context.SaveChangesAsync();

            return cca_line_printer;
        }

        private bool CcaLinePrinterExists(long id)
        {
            return _context.CcaLinePrinter.Any(e => e.Seqid == id);
        }
    }
}
