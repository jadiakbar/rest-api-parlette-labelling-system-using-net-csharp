﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlsAPI.Models;

namespace PlsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CcaDevicesController : ControllerBase
    {
        private readonly CCAIContext _context;

        public CcaDevicesController(CCAIContext context)
        {
            _context = context;
        }

        // GET: api/CcaDevices
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CcaDevices>>> GetCcaDevices()
        {
            return await _context.CcaDevices.ToListAsync();
        }

        // GET: api/CcaDevices/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CcaDevices>> GetCcaDevices(int id)
        {
            var ccaDevices = await _context.CcaDevices.FindAsync(id);

            if (ccaDevices == null)
            {
                return NotFound();
            }

            return ccaDevices;
        }

        // PUT: api/CcaDevices/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCcaDevices(int id, CcaDevices ccaDevices)
        {
            if (id != ccaDevices.DeviceId)
            {
                return BadRequest();
            }

            _context.Entry(ccaDevices).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CcaDevicesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CcaDevices
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<CcaDevices>> PostCcaDevices(CcaDevices ccaDevices)
        {
            _context.CcaDevices.Add(ccaDevices);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCcaDevices", new { id = ccaDevices.DeviceId }, ccaDevices);
        }

        // DELETE: api/CcaDevices/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CcaDevices>> DeleteCcaDevices(int id)
        {
            var ccaDevices = await _context.CcaDevices.FindAsync(id);
            if (ccaDevices == null)
            {
                return NotFound();
            }

            _context.CcaDevices.Remove(ccaDevices);
            await _context.SaveChangesAsync();

            return ccaDevices;
        }

        private bool CcaDevicesExists(int id)
        {
            return _context.CcaDevices.Any(e => e.DeviceId == id);
        }
    }
}
