﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlsAPI.Models;

namespace PlsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CcaLinesController : ControllerBase
    {
        private readonly CCAIContext _context;
        //CCAIContext db = new CCAIContext();

        public CcaLinesController(CCAIContext context)
        {
            _context = context;
        }

        // GET: api/CcaLines
        [HttpGet]
        /* public async Task<ActionResult<IEnumerable<CcaLine>>> GetCcaLine()
        {
            return await _context.CcaLine.ToListAsync();
        } */
        //public IQueryable<object> Index()
        //public IActionResult Get()
        public IQueryable<object> Index()
        {
            var cca_line = _context.CcaLineJoin
                /* .FromSqlRaw("SELECT cwi.plant_id PlantId, plant_descr PlantDescr, plant_code PlantCode, CONCAT(plant_descr, ' - ', plant_code) AS PlantArea , cw.line_id LineId, line_code LineCode, line_name LineName, cw.rec_created RecCreated, cw.rec_datetime RecDatetime FROM cca_line cw JOIN cca_plant_line cwi ON cwi.line_id = cw.line_id JOIN cca_plant cwp ON cwp.plant_id = cwi.plant_id ORDER BY line_name ASC") */
                .FromSqlRaw("SELECT DISTINCT a.line_id LineId, a.line_code LineCode,  a.line_name LineName, c.plant_id PlantId, c.plant_descr PlantDescr, c.plant_code PlantCode, CONCAT(c.plant_descr, ' - ', c.plant_code) AS PlantArea, d.is_repack IsRepack, d.sscc_code SsccCode FROM cca_line a INNER JOIN cca_plant_line b ON a.line_id = b.line_id INNER JOIN cca_plant c ON b.plant_id = c.plant_id LEFT JOIN cca_line_repack d ON a.line_id = d.line_id ORDER BY a.line_id DESC")
                .Select(x => new
                {
                    PlantId = x.PlantId,
                    PlantDescr = x.PlantDescr,
                    PlantCode = x.PlantCode,
                    PlantArea = x.PlantArea,
                    LineId = x.LineId,
                    LineCode = x.LineCode,
                    LineName = x.LineName,
                    IsRepack = x.IsRepack,
                    SsccCode = x.SsccCode
                });
            return cca_line;

            //return cca_line.ToList();
        }

        [HttpGet("Last")]
        public IQueryable<object> Last()
        {
            var cca_line = _context.CcaLineJoin
                /* .FromSqlRaw("SELECT TOP 1 cwi.plant_id PlantId, plant_descr PlantDescr, plant_code PlantCode, CONCAT(plant_descr, ' - ', plant_code) AS PlantArea , cw.line_id LineId, line_code LineCode, line_name LineName, cw.rec_created RecCreated, cw.rec_datetime RecDatetime FROM cca_line cw JOIN cca_plant_line cwi ON cwi.line_id = cw.line_id JOIN cca_plant cwp ON cwp.plant_id = cwi.plant_id ORDER BY cw.line_id DESC") */
                .FromSqlRaw("SELECT TOP 1 line_id LineId, line_code LineCode, line_name LineName FROM cca_line ORDER BY line_id DESC")
                .Select(x => new
                {
                    LineId = x.LineId,
                    LineCode = x.LineCode,
                    LineName = x.LineName
                });
            return cca_line;
        }

        // GET: api/CcaLines/5
        [HttpGet("{id}")]
        //public async Task<ActionResult<CcaLine>> GetCcaLine(int id)
        public IQueryable<object> GetCcaLine(int id)
        {
            /*var ccaLine = await _context.CcaLine.FindAsync(id);

            if (ccaLine == null)
            {
                return NotFound();
            } */

            //return ccaLine;
            var cca_line = _context.CcaLineJoin
                /*.FromSqlRaw("SELECT cwi.plant_id PlantId, plant_descr PlantDescr, plant_code PlantCode, CONCAT(plant_descr, ' - ', plant_code) AS PlantArea , cw.line_id LineId, line_code LineCode, line_name LineName, cw.rec_created RecCreated, cw.rec_datetime RecDatetime FROM cca_line cw JOIN cca_plant_line cwi ON cwi.line_id = cw.line_id JOIN cca_plant cwp ON cwp.plant_id = cwi.plant_id WHERE cw.line_id = " + id+" ORDER BY line_name ASC") */
                .FromSqlRaw("SELECT TOP 1 a.line_id LineId, a.line_code LineCode,  a.line_name LineName, c.plant_id PlantId, c.plant_descr PlantDescr, c.plant_code PlantCode, CONCAT(c.plant_descr, ' - ', c.plant_code) AS PlantArea, d.is_repack IsRepack, d.sscc_code SsccCode FROM cca_line a INNER JOIN cca_plant_line b ON a.line_id = b.line_id INNER JOIN cca_plant c ON b.plant_id = c.plant_id LEFT JOIN cca_line_repack d ON a.line_id = d.line_id WHERE a.line_id = "+id)
                .Select(x => new
                {
                    PlantId = x.PlantId,
                    PlantDescr = x.PlantDescr,
                    PlantCode = x.PlantCode,
                    PlantArea = x.PlantArea,
                    LineId = x.LineId,
                    LineCode = x.LineCode,
                    LineName = x.LineName,
                    IsRepack = x.IsRepack,
                    SsccCode = x.SsccCode
                });

            return cca_line;
        }

        // PUT: api/CcaLines/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCcaLine(int id, CcaLine ccaLine)
        {
            if (id != ccaLine.LineId)
            {
                return BadRequest();
            }

            _context.Entry(ccaLine).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CcaLineExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }



        [HttpGet("GetCcaLineForPost/{id}")]
        public async Task<ActionResult<CcaLine>> GetCcaLineForPost(int id)
        {
            var ccaLine = await _context.CcaLine.FirstOrDefaultAsync(m => m.LineId == id);

            if (ccaLine == null)
            {
                return NotFound();
            }

            return ccaLine;
        }

        // POST: api/CcaLines
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        /* public async Task<ActionResult<CcaLine>> PostCcaLine(CcaLine ccaLine)
        {
            _context.CcaLine.Add(ccaLine);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCcaLineForPost", new { id = ccaLine.LineId }, ccaLine);
        } */
        public async Task<ActionResult<CcaLine>> PostCcaLine(CcaLine ccaLine)
        {
            _context.CcaLine.Add(ccaLine);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCcaLineForPost", new { id = ccaLine.LineId }, ccaLine);
        }

        // DELETE: api/CcaLines/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CcaLine>> DeleteCcaLine(int id)
        {
            var ccaLine = await _context.CcaLine.FindAsync(id);
            if (ccaLine == null)
            {
                return NotFound();
            }

            _context.CcaLine.Remove(ccaLine);
            await _context.SaveChangesAsync();

            return ccaLine;
        }

        private bool CcaLineExists(int id)
        {
            return _context.CcaLine.Any(e => e.LineId == id);
        }
    }
}
