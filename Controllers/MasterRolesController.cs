﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlsAPI.Models;

namespace PlsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MasterRolesController : ControllerBase
    {
        private readonly CCAIContext _context;
        //CCAIContext db = new CCAIContext();

        public MasterRolesController(CCAIContext context)
        {
            _context = context;
        }

        // GET: api/MasterRoles
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MasterRole>>> GetMasterRole()
        {
            return await _context.MasterRole.ToListAsync();
        }
        /*
        public IQueryable<object> Index()
        {
            return from a in db.MasterRole
                   join b in db.MasterRolePlant on a.RoleId equals b.RoleId
                   where b.PlantId == 13
                   orderby a.RoleId, a.RoleName
                   select new
                   {
                       RoleId = a.RoleId,
                       RoleName = a.RoleName
                   };
        }*/

        // GET: api/MasterRoles/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MasterRole>> GetMasterRole(int id)
        {
            var masterRole = await _context.MasterRole.FindAsync(id);

            if (masterRole == null)
            {
                return NotFound();
            }

            return masterRole;
        }

        // PUT: api/MasterRoles/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMasterRole(int id, MasterRole masterRole)
        {
            if (id != masterRole.RoleId)
            {
                return BadRequest();
            }

            _context.Entry(masterRole).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MasterRoleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MasterRoles
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<MasterRole>> PostMasterRole(MasterRole masterRole)
        {
            _context.MasterRole.Add(masterRole);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMasterRole", new { id = masterRole.RoleId }, masterRole);
        }

        // DELETE: api/MasterRoles/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MasterRole>> DeleteMasterRole(int id)
        {
            var masterRole = await _context.MasterRole.FindAsync(id);
            if (masterRole == null)
            {
                return NotFound();
            }

            _context.MasterRole.Remove(masterRole);
            await _context.SaveChangesAsync();

            return masterRole;
        }

        private bool MasterRoleExists(int id)
        {
            return _context.MasterRole.Any(e => e.RoleId == id);
        }
    }
}
