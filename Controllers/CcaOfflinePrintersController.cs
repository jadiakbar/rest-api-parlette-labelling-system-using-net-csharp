﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlsAPI.Models;

namespace PlsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CcaOfflinePrintersController : ControllerBase
    {
        private readonly CCAIContext _context;

        public CcaOfflinePrintersController(CCAIContext context)
        {
            _context = context;
        }

        // GET: api/CcaOfflinePrinters
        [HttpGet]
        public List<CcaPrinter> Index()
        {
            var cca_printer = _context.CcaPrinter
                .FromSqlRaw("SELECT a.* FROM cca_printer a JOIN cca_printer_plant b ON a.printer_id = b.printer_id AND a.is_online = 0 ORDER BY a.printer_name ASC"); ;
            return cca_printer.ToList();
        }

        // GET: api/CcaOfflinePrinters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CcaPrinter>> GetCcaPrinter(int id)
        {
            var ccaPrinter = await _context.CcaPrinter.FindAsync(id);

            if (ccaPrinter == null)
            {
                return NotFound();
            }

            return ccaPrinter;
        }

        // PUT: api/CcaOfflinePrinters/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCcaPrinter(int id, CcaPrinter ccaPrinter)
        {
            if (id != ccaPrinter.PrinterId)
            {
                return BadRequest();
            }

            _context.Entry(ccaPrinter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CcaPrinterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CcaOfflinePrinters
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<CcaPrinter>> PostCcaPrinter(CcaPrinter ccaPrinter)
        {
            _context.CcaPrinter.Add(ccaPrinter);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCcaPrinter", new { id = ccaPrinter.PrinterId }, ccaPrinter);
        }

        // DELETE: api/CcaOfflinePrinters/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CcaPrinter>> DeleteCcaPrinter(int id)
        {
            var ccaPrinter = await _context.CcaPrinter.FindAsync(id);
            if (ccaPrinter == null)
            {
                return NotFound();
            }

            _context.CcaPrinter.Remove(ccaPrinter);
            await _context.SaveChangesAsync();

            return ccaPrinter;
        }

        private bool CcaPrinterExists(int id)
        {
            return _context.CcaPrinter.Any(e => e.PrinterId == id);
        }
    }
}
