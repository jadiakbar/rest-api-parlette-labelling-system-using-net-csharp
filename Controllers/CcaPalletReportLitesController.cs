﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlsAPI.Models;

namespace PlsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CcaPalletReportLitesController : ControllerBase
    {
        private readonly CCAIContext _context;

        public CcaPalletReportLitesController(CCAIContext context)
        {
            _context = context;
        }

        // GET: api/CcaPalletReportLites
        [HttpGet]
        [DisableRequestSizeLimit]
        /* public async Task<ActionResult<IEnumerable<CcaPalletReportLite>>> GetCcaPalletReportLite()
        {
            return await _context.CcaPalletReportLite.ToListAsync();
        } */
        public IQueryable<object> Index()
        {
            var cca_pallet_report_lite = _context.CcaPalletReportLiteJoin
                .FromSqlRaw("SELECT cpr.sscc_id SsccId, cpr.print_created PrintCreated, ISNULL(cpr.scan_created, '')  ScanCreated, cpr.pallet_no PalletNo, cpr.blocked_stock BlockedStock, cpr.sscc_reprinted SsccReprinted, cpr.reprinted_reason ReprintedReason, cpr.printreason_code PrintreasonCode, cpr.printreason_name PrintreasonName, cpr.rec_user RecUser, cpr.session_id SessionId, cpr.seqid Seqid, cws.workorder_id WorkorderId, cw.workorder_no WorkorderNo, cw.item_key ItemKey, cw.batch_no BatchNo, cw.item_qty ItemQty, cw.prod_date ProdDate, cw.expected_prod_date ExpectedProdDate, cw.expected_case ExpectedCase, cw.expected_pallets ExpectedPallets, csscc.sscc_number SsccNumber, csscc.sscc_qty SsccQty, cscontent.plant_name PlantName, cscontent.pallet_number PalletNumber, cscontent.item_material_no ItemMaterialNo, cscontent.prod_date ProdDateContent, cscontent.prod_code ProdCode, cscontent.article_name ArticleName, cscontent.article_qty ArticleQty, cscontent.best_before_date BestBeforeDate, cscontent.batch_no BatchNoContent, cscontent.sscc_number SsccNumberContent, cscontent.barcode_token BarcodeToken, ISNULL(csr.sscc_id, '') SsccIdCsr, ISNULL(csr.is_reprinted, '') IsReprinted, ISNULL(csr.rec_user, '') RecUserCsr, ISNULL(csr.rec_created, '') RecCreated FROM cca_workorder cw INNER JOIN cca_workorder_sscc cws ON cw.workorder_id = cws.workorder_id INNER JOIN cca_pallet_report cpr ON cws.sscc_id = cpr.sscc_id INNER JOIN cca_sscc csscc ON csscc.sscc_id = cws.sscc_id INNER JOIN cca_sscc_content cscontent ON cscontent.sscc_id = cws.sscc_id INNER JOIN cca_sscc_reprinted csr ON csr.sscc_id = cws.sscc_id  ORDER BY cpr.sscc_id DESC ")
                .Select(x => new
                {
                    SsccId = x.SsccId,
                    PrintCreated = x.PrintCreated,
                    ScanCreated = x.ScanCreated,
                    PalletNo = x.PalletNo,
                    BlockedStock = x.BlockedStock,
                    SsccReprinted = x.SsccReprinted,
                    ReprintedReason = x.ReprintedReason,
                    PrintreasonCode = x.PrintreasonCode,
                    PrintreasonName = x.PrintreasonName,
                    RecUser = x.RecUser,
                    SessionId = x.SessionId,
                    Seqid = x.Seqid,
                    WorkorderId = x.WorkorderId,
                    WorkorderNo = x.WorkorderNo,
                    ItemKey = x.ItemKey,
                    BatchNo = x.BatchNo,
                    ItemQty = x.ItemQty,
                    ProdDate = x.ProdDate,
                    ExpectedProdDate = x.ExpectedProdDate,
                    ExpectedCase = x.ExpectedCase,
                    ExpectedPallets = x.ExpectedPallets,
                    SsccNumber = x.SsccNumber,
                    SsccQty = x.SsccQty,
                    PlantName = x.PlantName,
                    PalletNumber = x.PalletNumber,
                    ItemMaterialNo = x.ItemMaterialNo,
                    ProdCode = x.ProdCode,
                    ArticleName = x.ArticleName,
                    ArticleQty = x.ArticleQty,
                    BestBeforeDate = x.BestBeforeDate,
                    BarcodeToken = x.BarcodeToken,
                    IsReprinted = x.IsReprinted,
                    RecCreated = x.RecCreated
                });
            return cca_pallet_report_lite;
        }

        // GET: api/CcaPalletReportLites/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CcaPalletReportLite>> GetCcaPalletReportLite(long id)
        {
            var ccaPalletReportLite = await _context.CcaPalletReportLite.FindAsync(id);

            if (ccaPalletReportLite == null)
            {
                return NotFound();
            }

            return ccaPalletReportLite;
        }

        // PUT: api/CcaPalletReportLites/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCcaPalletReportLite(long id, CcaPalletReportLite ccaPalletReportLite)
        {
            if (id != ccaPalletReportLite.Seqid)
            {
                return BadRequest();
            }

            _context.Entry(ccaPalletReportLite).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CcaPalletReportLiteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CcaPalletReportLites
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<CcaPalletReportLite>> PostCcaPalletReportLite(CcaPalletReportLite ccaPalletReportLite)
        {
            _context.CcaPalletReportLite.Add(ccaPalletReportLite);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCcaPalletReportLite", new { id = ccaPalletReportLite.Seqid }, ccaPalletReportLite);
        }

        // DELETE: api/CcaPalletReportLites/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CcaPalletReportLite>> DeleteCcaPalletReportLite(long id)
        {
            var ccaPalletReportLite = await _context.CcaPalletReportLite.FindAsync(id);
            if (ccaPalletReportLite == null)
            {
                return NotFound();
            }

            _context.CcaPalletReportLite.Remove(ccaPalletReportLite);
            await _context.SaveChangesAsync();

            return ccaPalletReportLite;
        }

        private bool CcaPalletReportLiteExists(long id)
        {
            return _context.CcaPalletReportLite.Any(e => e.Seqid == id);
        }
    }
}
