﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlsAPI.Models;

namespace PlsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CcaLineScannersController : ControllerBase
    {
        private readonly CCAIContext _context;

        public CcaLineScannersController(CCAIContext context)
        {
            _context = context;
        }

        // GET: api/CcaLineScanners
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CcaLineScanner>>> GetCcaLineScanner()
        {
            return await _context.CcaLineScanner.ToListAsync();
        }

        // GET: api/CcaLineScanners/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CcaLineScanner>> GetCcaLineScanner(long id)
        {
            var ccaLineScanner = await _context.CcaLineScanner.FindAsync(id);

            if (ccaLineScanner == null)
            {
                return NotFound();
            }

            return ccaLineScanner;
        }

        // PUT: api/CcaLineScanners/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCcaLineScanner(long id, CcaLineScanner ccaLineScanner)
        {
            if (id != ccaLineScanner.Seqid)
            {
                return BadRequest();
            }

            _context.Entry(ccaLineScanner).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CcaLineScannerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CcaLineScanners
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<CcaLineScanner>> PostCcaLineScanner(CcaLineScanner ccaLineScanner)
        {
            _context.CcaLineScanner.Add(ccaLineScanner);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCcaLineScanner", new { id = ccaLineScanner.Seqid }, ccaLineScanner);
        }

        // DELETE: api/CcaLineScanners/5
        [HttpDelete("{id}")]
        /* public async Task<ActionResult<CcaLineScanner>> DeleteCcaLineScanner(long id)
        {
            var ccaLineScanner = await _context.CcaLineScanner.FindAsync(id);
            if (ccaLineScanner == null)
            {
                return NotFound();
            }

            _context.CcaLineScanner.Remove(ccaLineScanner);
            await _context.SaveChangesAsync();

            return ccaLineScanner;
        } */
        public async Task<ActionResult<CcaLineScanner>> DeleteCcaLineScanner(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cca_line_scanner = await _context.CcaLineScanner
            .FirstOrDefaultAsync(m => m.LineId == id);
            if (cca_line_scanner == null)
            {
                return NotFound();
            }

            _context.CcaLineScanner.Remove(cca_line_scanner);
            await _context.SaveChangesAsync();

            return cca_line_scanner;
        }

        [HttpDelete("SickScanner/{id}")]
        public async Task<ActionResult<CcaLineScanner>> DeleteCcaSickLineScanner(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cca_line_scanner = await _context.CcaLineScanner
            .FirstOrDefaultAsync(m => m.ScannerId == id);
            if (cca_line_scanner == null)
            {
                return NotFound();
            }

            _context.CcaLineScanner.Remove(cca_line_scanner);
            await _context.SaveChangesAsync();

            return cca_line_scanner;
        }

        private bool CcaLineScannerExists(long id)
        {
            return _context.CcaLineScanner.Any(e => e.Seqid == id);
        }
    }
}
