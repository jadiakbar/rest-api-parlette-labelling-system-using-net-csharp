﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlsAPI.Models;

namespace PlsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CcaPlantLinesController : ControllerBase
    {
        private readonly CCAIContext _context;

        public CcaPlantLinesController(CCAIContext context)
        {
            _context = context;
        }

        // GET: api/CcaPlantLines
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CcaPlantLine>>> GetCcaPlantLine()
        {
            return await _context.CcaPlantLine.ToListAsync();
        }

        // GET: api/CcaPlantLines/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CcaPlantLine>> GetCcaPlantLine(int id)
        {
            var ccaPlantLine = await _context.CcaPlantLine.FindAsync(id);

            if (ccaPlantLine == null)
            {
                return NotFound();
            }

            return ccaPlantLine;
        }

        // PUT: api/CcaPlantLines/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCcaPlantLine(int id, CcaPlantLine ccaPlantLine)
        {
            if (id != ccaPlantLine.Seqid)
            {
                return BadRequest();
            }

            _context.Entry(ccaPlantLine).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CcaPlantLineExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CcaPlantLines
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<CcaPlantLine>> PostCcaPlantLine(CcaPlantLine ccaPlantLine)
        {
            _context.CcaPlantLine.Add(ccaPlantLine);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCcaPlantLine", new { id = ccaPlantLine.Seqid }, ccaPlantLine);
        }

        // DELETE: api/CcaPlantLines/5
        [HttpDelete("{id}")]
        /*public async Task<ActionResult<CcaPlantLine>> DeleteCcaPlantLine(int id)
        {
            var ccaPlantLine = await _context.CcaPlantLine.FindAsync(id);
            if (ccaPlantLine == null)
            {
                return NotFound();
            }

            _context.CcaPlantLine.Remove(ccaPlantLine);
            await _context.SaveChangesAsync();

            return ccaPlantLine;
        } */
        public async Task<ActionResult<CcaPlantLine>> DeleteCcaPlantLine(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cca_plant_line = await _context.CcaPlantLine
            .FirstOrDefaultAsync(m => m.LineId == id);
            if (cca_plant_line == null)
            {
                return NotFound();
            }

            _context.CcaPlantLine.Remove(cca_plant_line);
            await _context.SaveChangesAsync();

            return cca_plant_line;
        }

        private bool CcaPlantLineExists(int id)
        {
            return _context.CcaPlantLine.Any(e => e.Seqid == id);
        }
    }
}
