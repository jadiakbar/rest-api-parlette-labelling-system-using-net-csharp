﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlsAPI.Models;

namespace PlsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CcaLineRepacksController : ControllerBase
    {
        private readonly CCAIContext _context;

        public CcaLineRepacksController(CCAIContext context)
        {
            _context = context;
        }

        // GET: api/CcaLineRepacks
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CcaLineRepack>>> GetCcaLineRepack()
        {
            return await _context.CcaLineRepack.ToListAsync();
        }

        // GET: api/CcaLineRepacks/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CcaLineRepack>> GetCcaLineRepack(long id)
        {
            var ccaLineRepack = await _context.CcaLineRepack.FindAsync(id);

            if (ccaLineRepack == null)
            {
                return NotFound();
            }

            return ccaLineRepack;
        }

        // PUT: api/CcaLineRepacks/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCcaLineRepack(long id, CcaLineRepack ccaLineRepack)
        {
            if (id != ccaLineRepack.Seqid)
            {
                return BadRequest();
            }

            _context.Entry(ccaLineRepack).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CcaLineRepackExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CcaLineRepacks
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<CcaLineRepack>> PostCcaLineRepack(CcaLineRepack ccaLineRepack)
        {
            _context.CcaLineRepack.Add(ccaLineRepack);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCcaLineRepack", new { id = ccaLineRepack.Seqid }, ccaLineRepack);
        }

        // DELETE: api/CcaLineRepacks/5
        [HttpDelete("{id}")]
        /* public async Task<ActionResult<CcaLineRepack>> DeleteCcaLineRepack(long id)
        {
            var ccaLineRepack = await _context.CcaLineRepack.FindAsync(id);
            if (ccaLineRepack == null)
            {
                return NotFound();
            }

            _context.CcaLineRepack.Remove(ccaLineRepack);
            await _context.SaveChangesAsync();

            return ccaLineRepack;
        } */
        public async Task<ActionResult<CcaLineRepack>> DeleteCcaLineRepack(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cca_line_repack = await _context.CcaLineRepack
            .FirstOrDefaultAsync(m => m.LineId == id);
            if (cca_line_repack == null)
            {
                return NotFound();
            }

            _context.CcaLineRepack.Remove(cca_line_repack);
            await _context.SaveChangesAsync();

            return cca_line_repack;
        }

        private bool CcaLineRepackExists(long id)
        {
            return _context.CcaLineRepack.Any(e => e.Seqid == id);
        }
    }
}
