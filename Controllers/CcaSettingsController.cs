﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlsAPI.Models;

namespace PlsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CcaSettingsController : ControllerBase
    {
        private readonly CCAIContext _context;

        public CcaSettingsController(CCAIContext context)
        {
            _context = context;
        }

        // GET: api/CcaSettings
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CcaSettings>>> GetCcaSettings()
        {
            return await _context.CcaSettings.ToListAsync();
        }

        // GET: api/CcaSettings/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CcaSettings>> GetCcaSettings(long id)
        {
            var ccaSettings = await _context.CcaSettings.FindAsync(id);

            if (ccaSettings == null)
            {
                return NotFound();
            }

            return ccaSettings;
        }

        // PUT: api/CcaSettings/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCcaSettings(long id, CcaSettings ccaSettings)
        {
            if (id != ccaSettings.Seqid)
            {
                return BadRequest();
            }

            _context.Entry(ccaSettings).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CcaSettingsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CcaSettings
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<CcaSettings>> PostCcaSettings(CcaSettings ccaSettings)
        {
            _context.CcaSettings.Add(ccaSettings);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCcaSettings", new { id = ccaSettings.Seqid }, ccaSettings);
        }

        // DELETE: api/CcaSettings/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CcaSettings>> DeleteCcaSettings(long id)
        {
            var ccaSettings = await _context.CcaSettings.FindAsync(id);
            if (ccaSettings == null)
            {
                return NotFound();
            }

            _context.CcaSettings.Remove(ccaSettings);
            await _context.SaveChangesAsync();

            return ccaSettings;
        }

        private bool CcaSettingsExists(long id)
        {
            return _context.CcaSettings.Any(e => e.Seqid == id);
        }
    }
}
