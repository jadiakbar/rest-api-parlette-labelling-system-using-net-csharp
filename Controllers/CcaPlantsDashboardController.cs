﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlsAPI.Models;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PlsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CcaPlantsDashboardController : ControllerBase
    {
        private readonly CCAIContext _context;
        //CCAIContext db = new CCAIContext();

        public CcaPlantsDashboardController(CCAIContext context)
        {
            _context = context;
        }

        // GET: api/CcaPlantsDashboard
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CcaPlant>>> GetCcaLine()
        {
            return await _context.CcaPlant.ToListAsync();
        }

        //public IEnumerable<CcaPlant> GetCcaPlantsAsync()
        /*public IQueryable<object> Index()
        {
            return from a in db.CcaItem
                join p in db.CcaItemPlant on a.ItemKey equals p.ItemKey
                where a.ItemKey == 846
                select new
                {
                    name = a.ItemName
                };
        }*/

        // GET: api/CcaPlantsDashboard/5
        [HttpGet("{id}")]
        /*public async Task<ActionResult<CcaPlant>> GetCcaPlant(int id)
        {
            var ccaPlant = await _context.CcaPlant.FindAsync(id);
            if (ccaPlant == null)
            {
                return NotFound();
            }
            return ccaPlant;
            
            /*
            using (DBEntities db = new DBEntities())
            {
                List<MasterRolePlant> master_role_plant = db.MasterRolePlant.ToList();
                List<MasterRole> master_role = db.MasterRole.ToList();
                List<CcaPlant> cca_plant = db.CcaPlant.ToList();

                var SeletPlant = from e in employees
                                     join d in departments on e.Department_Id equals d.DepartmentId into table1
                                     from d in table1.ToList()
                                     join i in incentives on e.Incentive_Id equals i.IncentiveId into table2
                                     from i in table2.ToList()
                                     select new ViewModel
                                     {
                                         employee = e,
                                         department = d,
                                         incentive = i
                                     };
                return View(employeeRecord);
            }
            */
        //}

        // PUT: api/CcaPlantsDashboard/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCcaPlant(int id, CcaPlant ccaPlant)
        {
            if (id != ccaPlant.PlantId)
            {
                return BadRequest();
            }

            _context.Entry(ccaPlant).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CcaPlantExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CcaPlantsDashboard
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<CcaPlant>> PostCcaPlant(CcaPlant ccaPlant)
        {
            _context.CcaPlant.Add(ccaPlant);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCcaPlant", new { id = ccaPlant.PlantId }, ccaPlant);
        }

        // DELETE: api/CcaPlantsDashboard/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CcaPlant>> DeleteCcaPlant(int id)
        {
            var ccaPlant = await _context.CcaPlant.FindAsync(id);
            if (ccaPlant == null)
            {
                return NotFound();
            }

            _context.CcaPlant.Remove(ccaPlant);
            await _context.SaveChangesAsync();

            return ccaPlant;
        }

        private bool CcaPlantExists(int id)
        {
            return _context.CcaPlant.Any(e => e.PlantId == id);
        }
    }
}
