﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlsAPI.Models;

namespace PlsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CcaScannersController : ControllerBase
    {
        private readonly CCAIContext _context;

        public CcaScannersController(CCAIContext context)
        {
            _context = context;
        }

        // GET: api/CcaScanners
        [HttpGet]
        /* public async Task<ActionResult<IEnumerable<CcaScanner>>> GetCcaScanner()
        {
            return await _context.CcaScanner.ToListAsync();
        } */
        public List<CcaScanner> Index()
        {
            var cca_scanner = _context.CcaScanner
                .FromSqlRaw("SELECT * FROM cca_scanner ORDER BY scanner_id DESC");
            return cca_scanner.ToList();
        }

        [HttpGet("Last")]
        public List<CcaScanner> Last()
        {
            var cca_scanner = _context.CcaScanner
                .FromSqlRaw("SELECT TOP 1 * FROM cca_scanner ORDER BY scanner_id DESC");
            return cca_scanner.ToList();
        }

        // GET: api/CcaScanners/5
        [HttpGet("{id}")]
        /* public async Task<ActionResult<CcaScanner>> GetCcaScanner(int id)
        {
            var ccaScanner = await _context.CcaScanner.FindAsync(id);

            if (ccaScanner == null)
            {
                return NotFound();
            }

            return ccaScanner;
        } */
        public IQueryable<object> GetCcaScannerById(int id)
        {
            var cca_scanner = _context.CcaScanner
                .FromSqlRaw("SELECT TOP 1 * FROM cca_scanner WHERE scanner_id = " + id)
                .Select(x => new
                {
                    ScannerId = x.ScannerId,
                    ScannerName = x.ScannerName,
                    ScannerIp = x.ScannerIp,
                    ScannerPort = x.ScannerPort
                });

            return cca_scanner;
        }

        // PUT: api/CcaScanners/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCcaScanner(int id, CcaScanner ccaScanner)
        {
            if (id != ccaScanner.ScannerId)
            {
                return BadRequest();
            }

            _context.Entry(ccaScanner).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CcaScannerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CcaScanners
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<CcaScanner>> PostCcaScanner(CcaScanner ccaScanner)
        {
            _context.CcaScanner.Add(ccaScanner);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCcaScanner", new { id = ccaScanner.ScannerId }, ccaScanner);
        }

        // DELETE: api/CcaScanners/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CcaScanner>> DeleteCcaScanner(int id)
        {
            var ccaScanner = await _context.CcaScanner.FindAsync(id);
            if (ccaScanner == null)
            {
                return NotFound();
            }

            _context.CcaScanner.Remove(ccaScanner);
            await _context.SaveChangesAsync();

            return ccaScanner;
        }

        private bool CcaScannerExists(int id)
        {
            return _context.CcaScanner.Any(e => e.ScannerId == id);
        }
    }
}
