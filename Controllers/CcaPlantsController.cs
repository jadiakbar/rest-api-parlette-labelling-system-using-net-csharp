﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Configuration;
using System.Data.SqlClient;
using PlsAPI.Models;

namespace PlsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CcaPlantsController : ControllerBase
    {
        private readonly CCAIContext _context;

        public CcaPlantsController(CCAIContext context)
        {
            _context = context;
        }

        // GET: api/CcaPlants
        [HttpGet]
        /* public async Task<ActionResult<IEnumerable<CcaPlant>>> GetCcaPlant()
        {
            return await _context.CcaPlant.ToListAsync();
        } */
        public List<CcaPlant> Index()
        {
            var cca_plant = _context.CcaPlant
                .FromSqlRaw("SELECT * FROM cca_plant ORDER BY plant_id DESC");
            return cca_plant.ToList();
        }

        [HttpGet("Last")]
        public List<CcaPlant> Last()
        {
            var cca_plant = _context.CcaPlant
                .FromSqlRaw("SELECT TOP 1 * FROM cca_plant ORDER BY plant_id DESC");
            return cca_plant.ToList();
        }

        [HttpGet("GetCcaPlantbyId/{id}")]
        //public List<CcaPlant> Edit(int id)
        public IQueryable<object> GetCcaPlantbyId(int id)
        {
            var cca_plant = _context.CcaPlantJoin
                .FromSqlRaw("SELECT TOP 1 a.plant_id PlantId, a.plant_descr PlantDescr, a.plant_code PlantCode, b.is_interval IsInterval, b.gr_interval GrInterval, c.use_wms UseWms, d.use_ewm UseEwm FROM cca_plant a LEFT JOIN cca_plant_realtime b ON a.plant_id = b.plant_id LEFT JOIN cca_plant_wms c ON a.plant_id = c.plant_id LEFT JOIN cca_plant_ewm d ON a.plant_id = d.plant_id WHERE a.plant_id= "+id)
                .Select(x => new
                {
                    PlantId = x.PlantId,
                    PlantDescr = x.PlantDescr,
                    PlantCode = x.PlantCode,
                    IsInterval = x.IsInterval,
                    GrInterval = x.GrInterval,
                    UseWms = x.UseWms,
                    UseEwm = x.UseEwm
                });
            return cca_plant;
        }


        // GET: api/CcaPlants/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CcaPlant>> GetCcaPlant(int id)
        {
            var ccaPlant = await _context.CcaPlant.FindAsync(id);

            if (ccaPlant == null)
            {
                return NotFound();
            }

            return ccaPlant;
        }

        // PUT: api/CcaPlants/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCcaPlant(int id, CcaPlant ccaPlant)
        {
            if (id != ccaPlant.PlantId)
            {
                return BadRequest();
            }

            _context.Entry(ccaPlant).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CcaPlantExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CcaPlants
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<CcaPlant>> PostCcaPlant(CcaPlant ccaPlant)
        {
            _context.CcaPlant.Add(ccaPlant);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCcaPlant", new { id = ccaPlant.PlantId }, ccaPlant);
        }

        // DELETE: api/CcaPlants/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CcaPlant>> DeleteCcaPlant(int id)
        {
            var ccaPlant = await _context.CcaPlant.FindAsync(id);
            if (ccaPlant == null)
            {
                return NotFound();
            }

            _context.CcaPlant.Remove(ccaPlant);
            await _context.SaveChangesAsync();

            return ccaPlant;
        }

        private bool CcaPlantExists(int id)
        {
            return _context.CcaPlant.Any(e => e.PlantId == id);
        }
    }
}
