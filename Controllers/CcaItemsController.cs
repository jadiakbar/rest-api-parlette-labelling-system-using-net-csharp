﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlsAPI.Models;

namespace PlsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CcaItemsController : ControllerBase
    {
        private readonly CCAIContext _context;
        //CCAIContext db = new CCAIContext();

        public CcaItemsController(CCAIContext context)
        {
            _context = context;
        }

        // GET: api/CcaItems
        [HttpGet]
        /* public async Task<ActionResult<IEnumerable<CcaItem>>> GetCcaItem()
        {
            return await _context.CcaItem.ToListAsync();
        } */
        
        public IQueryable<object> Index()
        {
            PlsAPI.Models.CcaLine obj = new CcaLine();

            var cca_item = _context.CcaItemJoin
                /*.FromSqlRaw("SELECT ci.item_key ItemKey, item_name ItemName, item_sku ItemSku, item_material_no ItemMaterialNo, item_pallet_qty ItemPalletQty, item_pallet_type ItemPalletType,item_pallet_qty_2, item_pallet_type_2, item_best_before_in_days FROM cca_item ci JOIN cca_item_plant cip ON cip.item_key=ci.item_key WHERE plant_id=5 ORDER BY ci.item_key ASC") */
                .FromSqlRaw("SELECT ci.item_key ItemKey, item_name ItemName, item_sku ItemSku, item_material_no ItemMaterialNo, item_pallet_qty ItemPalletQty, item_pallet_type ItemPalletType, item_pallet_qty_2 ItemPalletQty2, item_pallet_type_2 ItemPalletType2, item_best_before_in_days ItemBestBeforeInDays FROM cca_item ci JOIN cca_item_plant cip ON cip.item_key=ci.item_key WHERE plant_id=5 ORDER BY ci.item_key ASC")
                .Select(x => new
                {
                    ItemKey = x.ItemKey,
                    ItemName = x.ItemName,
                    ItemSku = x.ItemSku,
                    ItemMaterialNo = x.ItemMaterialNo,
                    ItemPalletQty = x.ItemPalletQty,
                    ItemPalletType = x.ItemPalletType,
                    ItemPalletQty2 = x.ItemPalletQty2,
                    ItemPalletType2 = x.ItemPalletType2,
                    ItemBestBeforeInDays = x.ItemBestBeforeInDays
                });
            return cca_item;
        } 

        // GET: api/CcaItems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CcaItem>> GetCcaItem(long id)
        {
            var ccaItem = await _context.CcaItem.FindAsync(id);

            if (ccaItem == null)
            {
                return NotFound();
            }

            return ccaItem;
        }

        // PUT: api/CcaItems/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCcaItem(long id, CcaItem ccaItem)
        {
            if (id != ccaItem.ItemKey)
            {
                return BadRequest();
            }

            _context.Entry(ccaItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CcaItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CcaItems
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        /* public async Task<ActionResult<CcaItem>> PostCcaItem(CcaItem ccaItem)
        {
            _context.CcaItem.Add(ccaItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCcaItem", new { id = ccaItem.ItemKey }, ccaItem);
        } */
        public async Task<ActionResult<CcaItem>> PostCcaItem(CcaItem ccaItem)
        {
            /* using (var memoryStream = new MemoryStream())
            {
                await CcaItem.FormFile.CopyToAsync(memoryStream);

                // Upload the file if less than 2 MB
                if (memoryStream.Length < 2097152)
                {
                    var file = new AppFile()
                    {
                        Content = memoryStream.ToArray()
                    };

                    _dbContext.CcaItem.Add(file);

                    await _dbContext.SaveChangesAsync();
                }
                else
                {
                    ModelState.AddModelError("File", "The file is too large.");
                }
            }
            return CreatedAtAction("GetCcaItem", new { id = ccaItem.ItemKey }, ccaItem); */
            _context.CcaItem.Add(ccaItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCcaItem", new { id = ccaItem.ItemKey }, ccaItem);
        }

        // DELETE: api/CcaItems/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CcaItem>> DeleteCcaItem(long id)
        {
            var ccaItem = await _context.CcaItem.FindAsync(id);
            if (ccaItem == null)
            {
                return NotFound();
            }

            _context.CcaItem.Remove(ccaItem);
            await _context.SaveChangesAsync();

            return ccaItem;
        }

        private bool CcaItemExists(long id)
        {
            return _context.CcaItem.Any(e => e.ItemKey == id);
        }
    }
}
