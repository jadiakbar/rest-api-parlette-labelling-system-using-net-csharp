﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlsAPI.Models;

namespace PlsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CcaPlantEwmsController : ControllerBase
    {
        private readonly CCAIContext _context;

        public CcaPlantEwmsController(CCAIContext context)
        {
            _context = context;
        }

        // GET: api/CcaPlantEwms
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CcaPlantEwm>>> GetCcaPlantEwm()
        {
            return await _context.CcaPlantEwm.ToListAsync();
        }

        // GET: api/CcaPlantEwms/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CcaPlantEwm>> GetCcaPlantEwm(long id)
        {
            var ccaPlantEwm = await _context.CcaPlantEwm.FindAsync(id);

            if (ccaPlantEwm == null)
            {
                return NotFound();
            }

            return ccaPlantEwm;
        }

        // PUT: api/CcaPlantEwms/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCcaPlantEwm(long id, CcaPlantEwm ccaPlantEwm)
        {
            if (id != ccaPlantEwm.Seqid)
            {
                return BadRequest();
            }

            _context.Entry(ccaPlantEwm).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CcaPlantEwmExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CcaPlantEwms
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<CcaPlantEwm>> PostCcaPlantEwm(CcaPlantEwm ccaPlantEwm)
        {
            _context.CcaPlantEwm.Add(ccaPlantEwm);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCcaPlantEwm", new { id = ccaPlantEwm.Seqid }, ccaPlantEwm);
        }

        // DELETE: api/CcaPlantEwms/5
        [HttpDelete("{id}")]
        /* public async Task<ActionResult<CcaPlantEwm>> DeleteCcaPlantEwm(long id)
        {
            var ccaPlantEwm = await _context.CcaPlantEwm.FindAsync(id);
            if (ccaPlantEwm == null)
            {
                return NotFound();
            }

            _context.CcaPlantEwm.Remove(ccaPlantEwm);
            await _context.SaveChangesAsync();

            return ccaPlantEwm;
        } */
        public async Task<ActionResult<CcaPlantEwm>> DeleteCcaPlantEwm(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cca_plant_ewm = await _context.CcaPlantEwm
            .FirstOrDefaultAsync(m => m.PlantId == id);
            if (cca_plant_ewm == null)
            {
                return NotFound();
            }

            _context.CcaPlantEwm.Remove(cca_plant_ewm);
            await _context.SaveChangesAsync();

            return cca_plant_ewm;
        }

        private bool CcaPlantEwmExists(long id)
        {
            return _context.CcaPlantEwm.Any(e => e.Seqid == id);
        }
    }
}
