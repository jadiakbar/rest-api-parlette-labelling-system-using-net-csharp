﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Configuration;
using System.Data.SqlClient;
using PlsAPI.Models;

namespace PlsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CcaPlantRealtimesController : ControllerBase
    {
        private readonly CCAIContext _context;

        public CcaPlantRealtimesController(CCAIContext context)
        {
            _context = context;
        }

        // GET: api/CcaPlantRealtimes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CcaPlantRealtime>>> GetCcaPlantRealtime()
        {
            return await _context.CcaPlantRealtime.ToListAsync();
        }

        // GET: api/CcaPlantRealtimes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CcaPlantRealtime>> GetCcaPlantRealtime(long id)
        {
            var ccaPlantRealtime = await _context.CcaPlantRealtime.FindAsync(id);

            if (ccaPlantRealtime == null)
            {
                return NotFound();
            }

            return ccaPlantRealtime;
        }

        // PUT: api/CcaPlantRealtimes/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCcaPlantRealtime(long id, CcaPlantRealtime ccaPlantRealtime)
        {
            if (id != ccaPlantRealtime.Seqid)
            {
                return BadRequest();
            }

            _context.Entry(ccaPlantRealtime).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CcaPlantRealtimeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CcaPlantRealtimes
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<CcaPlantRealtime>> PostCcaPlantRealtime(CcaPlantRealtime ccaPlantRealtime)
        {
            _context.CcaPlantRealtime.Add(ccaPlantRealtime);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCcaPlantRealtime", new { id = ccaPlantRealtime.Seqid }, ccaPlantRealtime);
        }

        // DELETE: api/CcaPlantRealtimes/5
        [HttpDelete("{id}")]
        /*public async Task<ActionResult<CcaPlantRealtime>> DeleteCcaPlantRealtime(long id)
        {
            var ccaPlantRealtime = await _context.CcaPlantRealtime.FindAsync(id);
            if (ccaPlantRealtime == null)
            {
                return NotFound();
            }

            _context.CcaPlantRealtime.Remove(ccaPlantRealtime);
            await _context.SaveChangesAsync();

            return ccaPlantRealtime;
        } */
        public async Task<ActionResult<CcaPlantRealtime>> DeleteCcaPlantRealtime(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cca_plant_realtime = await _context.CcaPlantRealtime
            .FirstOrDefaultAsync(m => m.PlantId == id);
            if (cca_plant_realtime == null)
            {
                return NotFound();
            }

            _context.CcaPlantRealtime.Remove(cca_plant_realtime);
            await _context.SaveChangesAsync();

            return cca_plant_realtime;
        }

        private bool CcaPlantRealtimeExists(long id)
        {
            return _context.CcaPlantRealtime.Any(e => e.Seqid == id);
        }
    }
}
