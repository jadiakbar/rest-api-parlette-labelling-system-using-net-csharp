﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlsAPI.Models;

namespace PlsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CcaPlantWmsController : ControllerBase
    {
        private readonly CCAIContext _context;

        public CcaPlantWmsController(CCAIContext context)
        {
            _context = context;
        }

        // GET: api/CcaPlantWms
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CcaPlantWms>>> GetCcaPlantWms()
        {
            return await _context.CcaPlantWms.ToListAsync();
        }

        // GET: api/CcaPlantWms/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CcaPlantWms>> GetCcaPlantWms(long id)
        {
            var ccaPlantWms = await _context.CcaPlantWms.FindAsync(id);

            if (ccaPlantWms == null)
            {
                return NotFound();
            }

            return ccaPlantWms;
        }

        // PUT: api/CcaPlantWms/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCcaPlantWms(long id, CcaPlantWms ccaPlantWms)
        {
            if (id != ccaPlantWms.Seqid)
            {
                return BadRequest();
            }

            _context.Entry(ccaPlantWms).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CcaPlantWmsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CcaPlantWms
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<CcaPlantWms>> PostCcaPlantWms(CcaPlantWms ccaPlantWms)
        {
            _context.CcaPlantWms.Add(ccaPlantWms);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCcaPlantWms", new { id = ccaPlantWms.Seqid }, ccaPlantWms);
        }

        // DELETE: api/CcaPlantWms/5
        [HttpDelete("{id}")]
        /*public async Task<ActionResult<CcaPlantWms>> DeleteCcaPlantWms(long id)
        {
            var ccaPlantWms = await _context.CcaPlantWms.FindAsync(id);
            if (ccaPlantWms == null)
            {
                return NotFound();
            }

            _context.CcaPlantWms.Remove(ccaPlantWms);
            await _context.SaveChangesAsync();

            return ccaPlantWms;
        } */
        public async Task<ActionResult<CcaPlantWms>> DeleteCcaPlantWms(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cca_plant_wms = await _context.CcaPlantWms
            .FirstOrDefaultAsync(m => m.PlantId == id);
            if (cca_plant_wms == null)
            {
                return NotFound();
            }

            _context.CcaPlantWms.Remove(cca_plant_wms);
            await _context.SaveChangesAsync();

            return cca_plant_wms;
        }

        private bool CcaPlantWmsExists(long id)
        {
            return _context.CcaPlantWms.Any(e => e.Seqid == id);
        }
    }
}
