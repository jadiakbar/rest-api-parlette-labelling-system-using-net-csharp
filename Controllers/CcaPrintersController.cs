﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlsAPI.Models;

namespace PlsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CcaPrintersController : ControllerBase
    {
        private readonly CCAIContext _context;

        public CcaPrintersController(CCAIContext context)
        {
            _context = context;
        }

        // GET: api/CcaPrinters
        [HttpGet]
        /* public async Task<ActionResult<IEnumerable<CcaPrinter>>> GetCcaPrinter()
        {
            return await _context.CcaPrinter.ToListAsync();
        } */
        public List<CcaPrinter> Index()
        {
            var cca_printer = _context.CcaPrinter
                .FromSqlRaw("SELECT * FROM cca_printer ORDER BY printer_id DESC");
            return cca_printer.ToList();
        }

        // GET: api/CcaPrinters/5
        [HttpGet("{id}")]
        /* public async Task<ActionResult<CcaPrinter>> GetCcaPrinter(int id)
        {
            var ccaPrinter = await _context.CcaPrinter.FindAsync(id);

            if (ccaPrinter == null)
            {
                return NotFound();
            }

            return ccaPrinter;
        } */
        public IQueryable<object> GetCcaPriner(int id)
        {
            var cca_printer = _context.CcaPrinter
                .FromSqlRaw("SELECT TOP 1 printer_id, printer_name, printer_descr, printer_ip, printer_port, is_online FROM cca_printer WHERE printer_id = " + id)
                .Select(x => new
                {
                    PrinterId = x.PrinterId,
                    PrinterName = x.PrinterName,
                    PrinterDescr = x.PrinterDescr,
                    PrinterIp = x.PrinterIp,
                    PrinterPort = x.PrinterPort,
                    IsOnline = x.IsOnline
                });

            return cca_printer;
        }

        // PUT: api/CcaPrinters/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCcaPrinter(int id, CcaPrinter ccaPrinter)
        {
            if (id != ccaPrinter.PrinterId)
            {
                return BadRequest();
            }

            _context.Entry(ccaPrinter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CcaPrinterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CcaPrinters
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<CcaPrinter>> PostCcaPrinter(CcaPrinter ccaPrinter)
        {
            _context.CcaPrinter.Add(ccaPrinter);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCcaPrinter", new { id = ccaPrinter.PrinterId }, ccaPrinter);
        }

        // DELETE: api/CcaPrinters/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CcaPrinter>> DeleteCcaPrinter(int id)
        {
            var ccaPrinter = await _context.CcaPrinter.FindAsync(id);
            if (ccaPrinter == null)
            {
                return NotFound();
            }

            _context.CcaPrinter.Remove(ccaPrinter);
            await _context.SaveChangesAsync();

            return ccaPrinter;
        }

        private bool CcaPrinterExists(int id)
        {
            return _context.CcaPrinter.Any(e => e.PrinterId == id);
        }
    }
}
