﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlsAPI.Models;

namespace PlsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CcaWorkordersController : ControllerBase
    {
        private readonly CCAIContext _context;

        public CcaWorkordersController(CCAIContext context)
        {
            _context = context;
        }

        // GET: api/CcaWorkorders
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CcaWorkorder>>> GetCcaWorkorder()
        {
            return await _context.CcaWorkorder.ToListAsync();
        }

        // GET: api/CcaWorkorders/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CcaWorkorder>> GetCcaWorkorder(long id)
        {
            var ccaWorkorder = await _context.CcaWorkorder.FindAsync(id);

            if (ccaWorkorder == null)
            {
                return NotFound();
            }

            return ccaWorkorder;
        }

        // PUT: api/CcaWorkorders/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCcaWorkorder(long id, CcaWorkorder ccaWorkorder)
        {
            if (id != ccaWorkorder.WorkorderId)
            {
                return BadRequest();
            }

            _context.Entry(ccaWorkorder).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CcaWorkorderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CcaWorkorders
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<CcaWorkorder>> PostCcaWorkorder(CcaWorkorder ccaWorkorder)
        {
            _context.CcaWorkorder.Add(ccaWorkorder);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCcaWorkorder", new { id = ccaWorkorder.WorkorderId }, ccaWorkorder);
        }

        // DELETE: api/CcaWorkorders/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CcaWorkorder>> DeleteCcaWorkorder(long id)
        {
            var ccaWorkorder = await _context.CcaWorkorder.FindAsync(id);
            if (ccaWorkorder == null)
            {
                return NotFound();
            }

            _context.CcaWorkorder.Remove(ccaWorkorder);
            await _context.SaveChangesAsync();

            return ccaWorkorder;
        }

        private bool CcaWorkorderExists(long id)
        {
            return _context.CcaWorkorder.Any(e => e.WorkorderId == id);
        }
    }
}
