﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlsAPI.Models;

namespace PlsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MasterUsersController : ControllerBase
    {
        private readonly CCAIContext _context;
        //CCAIContext db = new CCAIContext();

        public MasterUsersController(CCAIContext context)
        {
            _context = context;
        }

        // GET: api/MasterUsers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MasterUser>>> GetMasterUser()
        {
            return await _context.MasterUser.ToListAsync();
        }

        /*
        public IQueryable<object> Index()
        {
            return from a in db.MasterUser
                   where Convert.ToInt32(a.UserStatus) == 1
                   orderby a.UserId
                   select new
                   {
                       UserId = a.UserId,
                       UserName = a.UserName,
                       UserFullName = a.UserFullname,
                       UserPhone = a.UserPhone,
                       UserEmail = a.UserEmail,
                       UserStatus = a.UserStatus
                   };
        }
        */

        // GET: api/MasterUsers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MasterUser>> GetMasterUser(int id)
        {
            var masterUser = await _context.MasterUser.FindAsync(id);

            if (masterUser == null)
            {
                return NotFound();
            }

            return masterUser;
        }

        // PUT: api/MasterUsers/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMasterUser(int id, MasterUser masterUser)
        {
            if (id != masterUser.UserId)
            {
                return BadRequest();
            }

            _context.Entry(masterUser).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MasterUserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MasterUsers
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<MasterUser>> PostMasterUser(MasterUser masterUser)
        {
            _context.MasterUser.Add(masterUser);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMasterUser", new { id = masterUser.UserId }, masterUser);
        }

        // DELETE: api/MasterUsers/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MasterUser>> DeleteMasterUser(int id)
        {
            var masterUser = await _context.MasterUser.FindAsync(id);
            if (masterUser == null)
            {
                return NotFound();
            }

            _context.MasterUser.Remove(masterUser);
            await _context.SaveChangesAsync();

            return masterUser;
        }

        private bool MasterUserExists(int id)
        {
            return _context.MasterUser.Any(e => e.UserId == id);
        }
    }
}
