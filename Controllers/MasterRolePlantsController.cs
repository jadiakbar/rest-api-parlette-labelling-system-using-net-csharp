﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlsAPI.Models;

namespace PlsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MasterRolePlantsController : ControllerBase
    {
        private readonly CCAIContext _context;

        public MasterRolePlantsController(CCAIContext context)
        {
            _context = context;
        }

        // GET: api/MasterRolePlants
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MasterRolePlant>>> GetMasterRolePlant()
        {
            return await _context.MasterRolePlant.ToListAsync();
        }

        // GET: api/MasterRolePlants/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MasterRolePlant>> GetMasterRolePlant(int id)
        {
            var masterRolePlant = await _context.MasterRolePlant.FindAsync(id);

            if (masterRolePlant == null)
            {
                return NotFound();
            }

            return masterRolePlant;
        }

        // PUT: api/MasterRolePlants/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMasterRolePlant(int id, MasterRolePlant masterRolePlant)
        {
            if (id != masterRolePlant.Seqid)
            {
                return BadRequest();
            }

            _context.Entry(masterRolePlant).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MasterRolePlantExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MasterRolePlants
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<MasterRolePlant>> PostMasterRolePlant(MasterRolePlant masterRolePlant)
        {
            _context.MasterRolePlant.Add(masterRolePlant);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMasterRolePlant", new { id = masterRolePlant.Seqid }, masterRolePlant);
        }

        // DELETE: api/MasterRolePlants/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MasterRolePlant>> DeleteMasterRolePlant(int id)
        {
            var masterRolePlant = await _context.MasterRolePlant.FindAsync(id);
            if (masterRolePlant == null)
            {
                return NotFound();
            }

            _context.MasterRolePlant.Remove(masterRolePlant);
            await _context.SaveChangesAsync();

            return masterRolePlant;
        }

        private bool MasterRolePlantExists(int id)
        {
            return _context.MasterRolePlant.Any(e => e.Seqid == id);
        }
    }
}
