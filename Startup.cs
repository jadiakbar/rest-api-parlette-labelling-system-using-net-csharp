using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using PlsAPI.Models;
using Microsoft.Data.SqlClient;
using System.Data.SqlClient;
using System.Configuration;

namespace PlsAPI
{
    public class Startup
    {
        /* public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        } */
        public Startup(Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddControllers
            // Add framework services.  
            services.AddMvc();
            //services.Add(new ServiceDescriptor(typeof(CCAIContext), new CCAIContext(Configuration.GetConnectionString("PlsAPIConnection"))));

            var connection = "Server=PC-NAME\\MSSQL2014;Database=db_name;Trusted_Connection=True;";
            services.AddDbContext<CCAIContext>(options => options.UseSqlServer(connection));

            //var connection = "Server=PC-NAME;Database=db_name;Trusted_Connection=True;";
            //services.AddDbContext<CCAIContext>(options => options.UseSqlServer(connection));
            /*
            services.AddDbContext<CCAIContext>(options => {
                options.UseSqlServer(Configuration.GetConnectionString("PlsAPIConnection"));
            });
            */


            //services.Add(new ServiceDescriptor(typeof(CCAIContext), new CCAIContext(Configuration.GetConnectionString("PlsAPIConnection"))));

            //services.AddTransient<AppDb>(_ => new AppDb(Configuration["ConnectionStrings:PlsAPIConnection"]));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }


            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
